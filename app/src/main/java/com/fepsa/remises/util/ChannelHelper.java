package com.fepsa.remises.util;

import android.annotation.TargetApi;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import com.fepsa.remises.R;

public class ChannelHelper {

    private static ChannelHelper INSTANCE;

    private Uri soundUri;
    private AudioAttributes audioAttrs;

    private ChannelHelper() {
        soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    }

    public static ChannelHelper getInstance() {

        if (INSTANCE == null)
            INSTANCE = new ChannelHelper();

        return INSTANCE;
    }

    public String createNotificationChannelDefault(NotificationManager notificationManager) {

        String channelId = "FEPSA_CHANNEL";

        if (Build.VERSION.SDK_INT < 26)
            return channelId;

        NotificationChannel channel = new NotificationChannel(
                channelId,
                "FEPSA",
                NotificationManager.IMPORTANCE_HIGH);

        channel.setLightColor(R.color.colorAccent);
        channel.setSound(soundUri, getAudioAttributes());
        channel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 500, 200, 500});

        notificationManager.createNotificationChannel(channel);

        return channelId;
    }

    @TargetApi(26)
    private AudioAttributes getAudioAttributes() {

        if (audioAttrs != null)
            return audioAttrs;

        else {

            AudioAttributes.Builder attrs = new AudioAttributes.Builder();
            attrs.setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION);
            attrs.setUsage(AudioAttributes.USAGE_NOTIFICATION);
            attrs.setUsage(AudioAttributes.USAGE_NOTIFICATION);

            audioAttrs = attrs.build();

            return audioAttrs;
        }
    }
}
