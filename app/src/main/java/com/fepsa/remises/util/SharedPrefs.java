package com.fepsa.remises.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fepsa.remises.ui.util.BaseFunctions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tksko on 02/07/2017.
 */

public class SharedPrefs {

    private static ObjectMapper mapper = BaseFunctions.getMapper();

    private static String TAG = "SharedPrefs";

    private static SharedPreferences preferences = null;
    private static SharedPreferences.Editor editor = null;

    public static class Constants {

        public static String PREFS_POSTFIX = "prefs";

        public static String PARAMETERS_DATA = "PARAMETERS_DATA";

        public static String PERSONS_DB_VERSION = "PERSONS_DB_VERSION";

        public static String GPS_INTERVAL = "GPS_INTERVAL";

        public static String LATEST_SAVED_DATE = "LATEST_SAVED_DATE";

        public static String CHOFER_TOKEN = "CHOFER_TOKEN";

        public static String CHOFER_ID = "CHOFER_ID";

        public static String ACTIVE_TRIP_ID = "ACTIVE_TRIP_ID";

        public static String PURGE_DB = "PURGE_DB";

    }

    public static void init(Context context) {

        preferences = context.getSharedPreferences(context.getPackageName()
                + Constants.PREFS_POSTFIX, Context.MODE_PRIVATE);

        editor = preferences.edit();
    }

    /*
    public static UserModel fetchUser() {

        return fetchObject(UserModel.class, Constants.USER);
    }

    public static void insertUser(UserModel user) {

        insertObjectAsJsonString(Constants.USER, user);
    }*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////  UTILS   ////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static String loadString(String key, String defValue) {
        return preferences.getString(key, defValue);
    }

    public static int loadInt(String key, int defValue) {
        return preferences.getInt(key, defValue);
    }

    public static long loadLong(String key, long defValue) {
        return preferences.getLong(key, defValue);
    }

    public static float loadFloat(String key, float defValue) {
        return preferences.getFloat(key, defValue);
    }

    public static boolean loadBoolean(String key, boolean defValue) {
        return preferences.getBoolean(key, defValue);
    }

    public static void saveString(String key, String value) {
        editor.putString(key, value);
        editor.apply();
    }

    public static void saveInt(String key, int value) {
        editor.putInt(key, value);
        editor.apply();
    }

    public static void saveLong(String key, long value) {
        editor.putLong(key, value);
        editor.apply();
    }

    public static void saveFloat(String key, float value) {
        editor.putFloat(key, value);
        editor.apply();
    }

    public static void saveBoolean(String key, boolean value) {
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static boolean containsPreference(String key) {
        return preferences.contains(key);
    }

    public static void clearPreference(String key) {
        editor.remove(key);
        editor.apply();
    }

    public static void clearPreferences() {
        editor.clear();
        editor.apply();
    }


    public static boolean fetchBoolean(String key) {

        return fetchBoolean(key, false);
    }

    public static boolean fetchBoolean(String key, boolean defaultValue) {

        boolean ret = defaultValue;

        if (containsPreference(key))
            ret = loadBoolean(key, defaultValue);

        return ret;
    }

    public static String fetchString(String key) {

        String ret = "";

        if (containsPreference(key))
            ret = loadString(key, "");

        return ret;
    }

    public static long fetchLong(String key) {

        long ret = 0;

        if (containsPreference(key))
            ret = loadLong(key, 0);

        return ret;
    }

    public static long fetchLong(String key, long defaultValue) {

        long ret = defaultValue;

        if (containsPreference(key))
            ret = loadLong(key, 0);

        return ret;
    }

    private static void removeElement(String key) {

        if (containsPreference(key))
            preferences.edit().remove(key).apply();

    }

    public static <T> T fetchObject(Class<T> className, String key) {

        if (containsPreference(key)) {

            String stringObject = loadString(key, "");

            if (TextUtils.isEmpty(stringObject))
                return null;

            try {
                return mapper.readValue(stringObject, className);

            } catch (IOException e) {

                e.printStackTrace();

                return null;
            }

        } else {

            return null;
        }
    }

    public static void insertObjectAsJsonString(String key, Object object) {

        String json = "";

        try {
            json = mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        saveString(key, json);
    }

    public static <T> List<T> fetchList(TypeReference<List<T>> typeRef, String key) {

        String json = loadString(key, "");

        if (!json.isEmpty()) {

            try {

                return mapper.readValue(json, typeRef);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return new ArrayList<>();
    }

}
