package com.fepsa.remises.util;

import com.fepsa.remises.business.model.ParametersModel;

/**
 * Created by tksko on 11/11/2017.
 */

public class ParametrosUtils {

    private static ParametrosUtils INSTANCE;

    private ParametersModel parametros;

    public static ParametrosUtils getInstance() {

        if (INSTANCE == null) {
            INSTANCE = new ParametrosUtils();
        }

        return INSTANCE;

    }

    public ParametrosUtils() {

        parametros = SharedPrefs.fetchObject(ParametersModel.class, SharedPrefs.Constants.PARAMETERS_DATA);

        if (parametros == null)
            parametros = new ParametersModel();

    }

    public ParametersModel getParametros() {
        return parametros;
    }

    public void setParametros(ParametersModel parametros) {

        this.parametros = parametros;

        SharedPrefs.insertObjectAsJsonString(SharedPrefs.Constants.PARAMETERS_DATA, parametros);

    }


    public int getGPSInterval() {

        return this.parametros.getGpsInterval();
    }

    public int getVersion() {

        return this.parametros.getVersionParametros();
    }

    public int getVersionPersonas() {

        return SharedPrefs.loadInt(SharedPrefs.Constants.PERSONS_DB_VERSION, -1);
    }

    public boolean personsNeedUpgrade() {

        return this.parametros.getVersionPersonas() != SharedPrefs.loadInt(SharedPrefs.Constants.PERSONS_DB_VERSION, -1);
    }

}
