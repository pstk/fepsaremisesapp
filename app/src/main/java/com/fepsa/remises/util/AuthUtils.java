package com.fepsa.remises.util;

/**
 * Created by tksko on 7/6/2017.
 */

/**
 * Created by tksko 06/07/2017.
 */

public class AuthUtils {

    public static void setChoferID(String token) {

        SharedPrefs.saveString(SharedPrefs.Constants.CHOFER_ID, token);
    }

    public static String getChoferID() {

        return SharedPrefs.loadString(SharedPrefs.Constants.CHOFER_ID, "");
    }

    public static void setToken(String token) {

        SharedPrefs.saveString(SharedPrefs.Constants.CHOFER_TOKEN, token);
    }

    public static String getToken() {

        return SharedPrefs.loadString(SharedPrefs.Constants.CHOFER_TOKEN, "");
    }

    public static boolean isLoggedIn() {

        return !SharedPrefs.loadString(SharedPrefs.Constants.CHOFER_TOKEN, "").equals("");
    }

    public static void logout() {

        SharedPrefs.clearPreferences();
    }

}