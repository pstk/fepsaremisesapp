package com.fepsa.remises.util;

/**
 * Created by tksko on 12/13/2017.
 */

public class TripUtils {

    private static TripUtils INSTANCE = null;

    private long tripId;

    public TripUtils() {

        tripId = SharedPrefs.fetchLong(SharedPrefs.Constants.ACTIVE_TRIP_ID, 0);

    }

    public static TripUtils getInstance() {

        if (INSTANCE == null) {
            INSTANCE = new TripUtils();
        }

        return INSTANCE;

    }

    public boolean isActiveTrip() {

        return this.tripId > 0;
    }

    public long getActiveTripId() {

        return tripId;

    }

    public void setActiveTripId(long tripId) {

        this.tripId = tripId;

        SharedPrefs.saveLong(SharedPrefs.Constants.ACTIVE_TRIP_ID, tripId);

    }

    public void clearActiveTrip() {

        SharedPrefs.clearPreference(SharedPrefs.Constants.ACTIVE_TRIP_ID);

    }

}
