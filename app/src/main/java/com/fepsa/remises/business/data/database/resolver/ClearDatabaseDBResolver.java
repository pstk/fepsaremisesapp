package com.fepsa.remises.business.data.database.resolver;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by tksko on 02/07/2017.
 */

public class ClearDatabaseDBResolver extends BaseDBResolver {

    public Observable<Boolean> executeQuery() {

        return Observable.create(
                new Observable.OnSubscribe<Boolean>() {

                    @Override
                    public void call(final Subscriber<? super Boolean> subscriber) {

                        realm.beginTransaction();

                        realm.deleteAll();

                        realm.commitTransaction();

                        subscriber.onNext(true);

                        subscriber.onCompleted();
                    }
                }
        )
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
