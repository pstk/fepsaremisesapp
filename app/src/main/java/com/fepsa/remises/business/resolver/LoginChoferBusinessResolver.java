package com.fepsa.remises.business.resolver;

import com.fepsa.remises.business.data.backend.BackendManager;
import com.fepsa.remises.business.data.backend.model.LoginEntity;
import com.fepsa.remises.util.AuthUtils;

/**
 * Created by tksko on 02/07/2017.
 */

public class LoginChoferBusinessResolver extends BaseBusinessResolver<Boolean, LoginEntity> {

    public LoginChoferBusinessResolver(BackendManager backendManager) {
        super(backendManager);
    }

    @Override
    protected void callBackend() {

        backendManager.loginChofer(
                String.valueOf(params.get("nombre")),
                String.valueOf(params.get("clave")),
                String.valueOf(params.get("dni"))
        ).subscribe(this);

    }

    @Override
    protected void onSuccessCallActions(LoginEntity response) {

        AuthUtils.setToken(response.getToken());

        AuthUtils.setChoferID(response.getChoferId());

        subscriber.onNext(true);

        subscriber.onCompleted();

    }

}
