package com.fepsa.remises.business.data.database.resolver;


import com.fepsa.remises.business.data.database.model.RealmPerson;

import java.util.List;

import io.realm.Case;
import io.realm.RealmResults;
import io.realm.Sort;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by tksko on 24/09/2017.
 */

public class GetPersonsByQueryDBResolver extends BaseDBResolver {

    public Observable<List<RealmPerson>> executeQuery(final String query, final int version) {

        return Observable.create(
                new Observable.OnSubscribe<List<RealmPerson>>() {

                    @Override
                    public void call(final Subscriber<? super List<RealmPerson>> subscriber) {

                        RealmResults<RealmPerson> people;

                        if (query == null || query.length() == 0) {
                            people = realm.where(RealmPerson.class).equalTo("local", false).equalTo("version", version).sort("name", Sort.ASCENDING).findAll();
                        } else {
                            people = realm.where(RealmPerson.class)
                                    .equalTo("local", false)
                                    .equalTo("version", version)
                                    .beginGroup()
                                    .contains("name", query, Case.INSENSITIVE)
                                    .or()
                                    .contains("legajo", query, Case.INSENSITIVE)
                                    .endGroup().sort("name", Sort.ASCENDING).findAll();
                        }

                        subscriber.onNext(people);

                        subscriber.onCompleted();

                    }

                }
        )
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
