package com.fepsa.remises.business.data.backend.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fepsa.remises.business.data.backend.model.SnappedPointEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tksko on 4/10/2017.
 */

@JsonIgnoreProperties(
        ignoreUnknown = true
)
public class SnapToRoadResponse extends BaseResponse {

    @JsonProperty("snappedPoints")
    private List<SnappedPointEntity> snappedPoints;

    public SnapToRoadResponse() {

        this.snappedPoints = new ArrayList<>();
    }

    public List<SnappedPointEntity> getSnappedPoints() {
        return snappedPoints;
    }

    public void setSnappedPoints(List<SnappedPointEntity> snappedPoints) {
        this.snappedPoints = snappedPoints;
    }
}
