package com.fepsa.remises.business.data.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by tksko on 4/12/2017.
 */

@JsonIgnoreProperties(
        ignoreUnknown = true
)
public class LoginEntity {

    @JsonProperty("token")
    private String token;

    @JsonProperty("chofer_id")
    private String choferId;

    @JsonProperty("nombre")
    private String nombre;

    public LoginEntity() {

        this.token = "";
        this.choferId = "";
        this.nombre = "";

    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getChoferId() {
        return choferId;
    }

    public void setChoferId(String choferId) {
        this.choferId = choferId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
