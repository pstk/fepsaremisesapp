package com.fepsa.remises.business.data.database.model;

import com.fepsa.remises.business.data.backend.model.PersonEntity;
import com.fepsa.remises.ui.model.PersonDTO;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by nicolas.m on 11/6/2017.
 */

public class RealmPerson extends RealmObject {

    private long id;

    private String name;

    @PrimaryKey
    private String legajo;

    private boolean local;

    private int version;

    public RealmPerson() {

        this.id = 0;
        this.legajo = "";
        this.name = "";
        this.local = false;
        this.version = -1;
    }

    public RealmPerson(PersonDTO person) {

        this.id = person.getId();
        this.legajo = person.getLegajo();
        this.name = person.getName();
        this.local = person.isLocal();
        this.version = person.getVersion();
    }

    public RealmPerson(PersonEntity person, int version) {

        this.id = person.getId();
        this.legajo = person.getLegajo();
        this.name = person.getName();
        this.local = false;
        this.version = version;
    }

    public static RealmList<RealmPerson> fromEntityList(List<PersonEntity> entities, int version) {

        RealmList<RealmPerson> res = new RealmList<>();

        if (entities == null) {

            return res;
        }

        for (PersonEntity entity : entities) {

            res.add(new RealmPerson(entity, version));
        }

        return res;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLegajo() {
        return legajo;
    }

    public void setLegajo(String legajo) {
        this.legajo = legajo;
    }

    public boolean isLocal() {
        return local;
    }

    public void setLocal(boolean local) {
        this.local = local;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}
