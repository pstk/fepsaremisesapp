package com.fepsa.remises.business.data.backend.resolver;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.crashlytics.android.Crashlytics;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fepsa.remises.business.data.backend.ApiService;
import com.fepsa.remises.business.data.backend.response.BaseErrorResponse;
import com.fepsa.remises.business.data.backend.util.BackendException;
import com.fepsa.remises.business.data.backend.util.CheckConnection;
import com.fepsa.remises.business.data.backend.util.RestClient;
import com.fepsa.remises.business.data.backend.util.ServerNotAvailableException;
import com.fepsa.remises.ui.application.App;
import com.fepsa.remises.ui.util.BaseFunctions;
import com.fepsa.remises.util.FileUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by tksko on 02/07/2017.
 */

public abstract class BaseBEResolver<T> implements Callback<T> {

    private static final String MULTIPART_FORM_DATA = "multipart/form-data";

    protected ApiService client = null;

    protected Subscriber<? super T> subscriber;

    private boolean useMockData = false;

    public BaseBEResolver() {

        this.useMockData = false;
        this.client = RestClient.INSTANCE.getApiService();
    }

    public Observable<T> makeCall(@Nullable final Map<String, Object> params) {

        return Observable.create(new Observable.OnSubscribe<T>() {

                                     @Override
                                     public void call(final Subscriber<? super T> subscriber) {

                                         BaseBEResolver.this.subscriber = subscriber;

                                         if (!isUseMockData()) {

                                             if (isNetworkConnectivityAvailable()) {

                                                 getDataFromBackend(params);
                                             } else {

                                                 subscriber.onError(new Exception("No network connectivity available"));
                                             }

                                         } else {

                                             getMockData(params);

                                         }

                                     }
                                 }
        )
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    protected abstract void getDataFromBackend(Map<String, Object> params);

    protected void getMockData(Map<String, Object> params) {

        subscriber.onError(new Exception("No getMockData() method implementation found. " +
                "Please override getMockData() in your resolver."));

    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {

        T baseResponse;

        try {

            if (response.isSuccessful()) {

                baseResponse = response.body();

                this.onSuccessResponse(baseResponse);

                subscriber.onNext(baseResponse);

                subscriber.onCompleted();

            } else {

                String jsonResponse = response.errorBody().string();

                BaseErrorResponse errorResponse = BaseFunctions.getMapper().readValue(jsonResponse, new TypeReference<BaseErrorResponse>() {
                });

                this.onFailResponse(errorResponse);

                subscriber.onError(new BackendException(errorResponse.getError()));

            }

        } catch (Exception e) {

            Crashlytics.logException(e);

            int code = response.code();

            if (code >= 500) {

                e = new ServerNotAvailableException();
            }

            this.onFailResponse(e);

            subscriber.onError(e);

        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {

        Crashlytics.logException(t);

        this.onFailResponse(t);

        subscriber.onError(t);
    }

    protected void onSuccessResponse(Object baseResponse) {
        // to be implemented by subclasses, when necessary
    }

    protected void onFailResponse(Object baseResponse) {
        // to be implemented by subclasses, when necessary
    }

    protected void onFailResponse(Throwable t) {
        // to be implemented by subclasses, when necessary
    }

    @NonNull
    protected RequestBody createStringPart(String descriptionString) {

        return RequestBody.create(
                MediaType.parse(MULTIPART_FORM_DATA), descriptionString);

    }

    @NonNull
    protected MultipartBody.Part createFilePart(String partName, Uri fileUri) {

        // create the file name
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String fileName = sdf.format(new Date());

        // create RequestBody instance from file
        RequestBody body =
                RequestBody.create(MediaType.parse(MULTIPART_FORM_DATA), FileUtils.getBytesFromUri(fileUri));

        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, fileName, body);

    }

    private boolean isNetworkConnectivityAvailable() {

        return CheckConnection.getInstance(App.applicationContext).isOnline();
    }

    public boolean isUseMockData() {
        return useMockData;
    }

    public void setUseMockData(boolean useMockData) {
        this.useMockData = useMockData;
    }

}
