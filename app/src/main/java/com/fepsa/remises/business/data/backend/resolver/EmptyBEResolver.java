package com.fepsa.remises.business.data.backend.resolver;

import com.fepsa.remises.business.data.backend.response.BaseResponse;

import java.util.Map;

/**
 * Created by tksko on 02/07/2017.
 */

public class EmptyBEResolver extends BaseBEResolver<BaseResponse> {

    @Override
    protected void getDataFromBackend(Map<String, Object> params) {

        subscriber.onCompleted();

    }

}
