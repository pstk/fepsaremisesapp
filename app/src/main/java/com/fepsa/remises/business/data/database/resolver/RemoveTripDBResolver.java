package com.fepsa.remises.business.data.database.resolver;


import com.fepsa.remises.business.data.database.model.RealmPassenger;
import com.fepsa.remises.business.data.database.model.RealmTrip;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by tksko on 24/09/2017.
 */

public class RemoveTripDBResolver extends BaseDBResolver {

    public Observable<Boolean> executeQuery(final long tripId) {

        return Observable.create(
                new Observable.OnSubscribe<Boolean>() {

                    @Override
                    public void call(final Subscriber<? super Boolean> subscriber) {

                        RealmTrip theTrip = realm.where(RealmTrip.class).equalTo("startDate", tripId).findFirst();

                        if (theTrip != null) {

                            realm.beginTransaction();

                            // Borramos todas las personas custom del viaje...
                            for (RealmPassenger passenger : theTrip.getPassengers()) {

                                if (passenger.getPerson().isLocal()) {
                                    passenger.getPerson().deleteFromRealm();
                                }

                            }

                            theTrip.getPassengers().deleteAllFromRealm();

                            theTrip.getLocations().deleteAllFromRealm();

                            theTrip.deleteFromRealm();

                            realm.commitTransaction();

                            subscriber.onNext(true);

                            subscriber.onCompleted();

                        } else {

                            // Pasajero no encontrado
                            subscriber.onError(new Exception("Viaje inválido."));

                            subscriber.onCompleted();

                        }

                    }
                }
        )
                .

                        subscribeOn(AndroidSchedulers.mainThread())
                .

                        observeOn(AndroidSchedulers.mainThread());
    }

}
