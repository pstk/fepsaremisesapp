package com.fepsa.remises.business.data.backend;

import com.fepsa.remises.business.data.backend.model.LoginEntity;
import com.fepsa.remises.business.data.backend.model.ParametersEntity;
import com.fepsa.remises.business.data.backend.response.BaseResponse;
import com.fepsa.remises.business.data.backend.response.SnapToRoadResponse;
import com.fepsa.remises.business.data.backend.response.UpdatePersonasResponse;
import com.fepsa.remises.config.AppConstants;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Created by tksko on 02/07/2017.
 */

public interface ApiService {

    @GET
    Call<SnapToRoadResponse> snapToRoad(
            @Url String url
    );

    // Login Chofer
    @POST(AppConstants.Endpoint.LOGIN_CHOFER)
    Call<LoginEntity> loginChofer(
            @Body RequestBody body
    );

    // Personas

    @GET(AppConstants.Endpoint.UPDATE_PERSONAS)
    Call<UpdatePersonasResponse> updatePersonas();

    // Parametros

    @GET(AppConstants.Endpoint.UPDATE_PARAMETERS)
    Call<ParametersEntity> updateParametros();

    // Viajes
    @Headers("Content-Type: application/json")
    @POST(AppConstants.Endpoint.UPLOAD_TRIP)
    Call<BaseResponse> uploadTrip(
            @Body RequestBody body
    );

}
