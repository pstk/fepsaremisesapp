package com.fepsa.remises.business.data.database.resolver;


import com.fepsa.remises.business.data.database.model.RealmLocation;
import com.fepsa.remises.business.data.database.model.RealmPassenger;
import com.fepsa.remises.business.data.database.model.RealmTrip;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by tksko on 24/09/2017.
 */

public class RemovePassengerDBResolver extends BaseDBResolver {

    public Observable<RealmPassenger> executeQuery(final long tripId, final String personLegajo, final RealmLocation location) {

        return Observable.create(
                new Observable.OnSubscribe<RealmPassenger>() {

                    @Override
                    public void call(final Subscriber<? super RealmPassenger> subscriber) {

                        RealmTrip theTrip = realm.where(RealmTrip.class).equalTo("startDate", tripId).findFirst();

                        RealmPassenger thePassenger = theTrip.getPassengers().where().equalTo("person.legajo", personLegajo).findFirst();

                        if (thePassenger != null) {

                            realm.beginTransaction();

                            RealmLocation theLocation = theTrip.getLocations().where().equalTo("time", location.getTime()).equalTo("tripId", tripId).findFirst();

                            if (theLocation == null) {

                                theLocation = realm.copyToRealm(location);

                                theTrip.getLocations().add(theLocation);

                            }

                            thePassenger.setPosBaja(theLocation);

                            realm.commitTransaction();

                            subscriber.onNext(thePassenger);

                            subscriber.onCompleted();

                        } else {

                            // Pasajero no encontrado
                            subscriber.onError(new Exception("Pasajero no encontrado"));

                            subscriber.onCompleted();

                        }

                    }
                }
        )
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
