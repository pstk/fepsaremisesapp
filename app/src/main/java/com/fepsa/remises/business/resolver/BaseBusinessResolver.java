package com.fepsa.remises.business.resolver;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import com.fepsa.remises.business.data.backend.BackendManager;
import com.fepsa.remises.business.data.backend.util.CheckConnection;
import com.fepsa.remises.business.data.database.DatabaseManager;
import com.fepsa.remises.config.AppConstants;
import com.fepsa.remises.ui.application.App;

import java.util.Map;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by tksko on 02/07/2017.
 */
public class BaseBusinessResolver<T, Z> extends Subscriber<Z> {

    protected BackendManager backendManager;

    protected DatabaseManager databaseManager;

    protected boolean forceBackend;

    protected Subscriber<? super T> subscriber;

    protected Map<String, Object> params;

    public BaseBusinessResolver(BackendManager backendManager) {

        this.backendManager = backendManager;

        this.forceBackend = false;
    }

    public BaseBusinessResolver(DatabaseManager databaseManager) {

        this.databaseManager = databaseManager;

        this.forceBackend = false;
    }

    public BaseBusinessResolver(DatabaseManager databaseManager, BackendManager backendManager) {

        this.databaseManager = databaseManager;

        this.backendManager = backendManager;

        this.forceBackend = false;
    }

    public Observable<T> getObservable(@Nullable final Map<String, Object> params) {

        return Observable.create(new Observable.OnSubscribe<T>() {

                                     @Override
                                     public void call(Subscriber<? super T> subscriber) {

                                         BaseBusinessResolver.this.subscriber = subscriber;

                                         BaseBusinessResolver.this.params = params;

                                         getObservableActionsFlow();

                                     }

                                 }
        );
    }

    protected void getObservableActionsFlow() {

        if (databaseManager != null) {

            callDatabase();
        } else if (!isNetworkConnectivityAvailable()) {

            subscriber.onError(new Exception("No hay conexión a internet."));
        } else if (backendManager != null) {

            callBackend();
        }

    }

    protected void callBackend() {

        subscriber.onCompleted();
    }

    protected void onSuccessCallActions(Z response) {

        subscriber.onCompleted();
    }

    protected void onFailureCallActions(Throwable e) {

        checkForBackendException(e);

        subscriber.onError(e);
    }

    protected void callDatabase() {

        subscriber.onCompleted();
    }

    /**
     * When set to true the server will be queried, regardless of the state of the local cache.
     * This is useful, for example when the user triggers a manual refresh of the data.
     */
    public void setForceBackend(boolean forceBackend) {

        this.forceBackend = forceBackend;
    }

    protected boolean isNetworkConnectivityAvailable() {

        return CheckConnection.getInstance(App.applicationContext).isOnline();
    }

    protected void checkForBackendException(Throwable e) {

    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {

        onFailureCallActions(e);

    }

    @Override
    public void onNext(Z response) {

        onSuccessCallActions(response);

    }

    protected void sendConnectionUnavailable() {

        Intent noAccessIntent = new Intent(AppConstants.BroadcastActions.NO_CONNECTION);

        noAccessIntent.putExtra("NETWORK_AVAILABLE", isNetworkConnectivityAvailable());

        LocalBroadcastManager.getInstance(App.applicationContext).sendBroadcast(noAccessIntent);

    }

}
