package com.fepsa.remises.business.data.database.resolver;

import com.fepsa.remises.business.data.database.model.RealmLocation;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by tksko on 24/09/2017.
 */

public class ClearLocationDBResolver extends BaseDBResolver {

    public Observable<Boolean> executeQuery() {

        return Observable.create(
                new Observable.OnSubscribe<Boolean>() {

                    @Override
                    public void call(final Subscriber<? super Boolean> subscriber) {

                        realm.beginTransaction();

                        realm.delete(RealmLocation.class);

                        realm.commitTransaction();

                        subscriber.onNext(true);

                        subscriber.onCompleted();
                    }
                }
        )
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
