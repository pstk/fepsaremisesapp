package com.fepsa.remises.business.data.database.model;

import android.location.Location;

import io.realm.RealmObject;

/**
 * Created by tksko on 24/9/2017.
 */

public class RealmLocation extends RealmObject {

    // the UTC time of this fix, in milliseconds since January 1, 1970.
    private long time = 0;

    private long tripId;

    /*
    // the time of this fix, in elapsed real-time since system boot.
    private long mElapsedRealtimeNanos = 0;
*/
    private double latitude = 0.0;

    private double longitude = 0.0;

    // the altitude if available, in meters above the WGS 84 reference ellipsoid.
    private double altitude = 0.0f;

    // the speed if it is available, in meters/second over ground.
    private float speed = 0.0f;

    // bearing is the horizontal direction of travel of this device, and is not related
    // to the device orientation. It is guaranteed to be in the range
    // (0.0, 360.0] if the device has a bearing. Return 0.0 if not availeable.
    private float bearing = 0.0f;

    // Get the estimated horizontal accuracy of this location, radial, in meters.
    // We define horizontal accuracy as the radius of 68% confidence. In other words,
    // if you draw a circle centered at this location's latitude and longitude,
    // and with a radius equal to the accuracy, then there is a 68% probability
    // that the true location is inside the circle.
    private float accuracy = 0.0f;

    private int paramsVersion;

    private String info;

    public RealmLocation() {
        time = 0;
        tripId = 0;
        latitude = 0;
        longitude = 0;
        altitude = 0;
        speed = 0;
        bearing = 0;
        accuracy = 0;
        paramsVersion = 0;
        info = "";
    }

    public RealmLocation(Location location, long tripId, int paramsVersion, String info) {
        time = location.getTime();
        this.tripId = tripId;
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        altitude = location.getAltitude();
        speed = location.getSpeed();
        bearing = location.getBearing();
        accuracy = location.getAccuracy();
        this.paramsVersion = paramsVersion;
        this.info = info;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getBearing() {
        return bearing;
    }

    public void setBearing(float bearing) {
        this.bearing = bearing;
    }

    public float getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(float accuracy) {
        this.accuracy = accuracy;
    }

    public long getTripId() {
        return tripId;
    }

    public void setTripId(long tripId) {
        this.tripId = tripId;
    }

    public int getParamsVersion() {
        return paramsVersion;
    }

    public void setParamsVersion(int paramsVersion) {
        this.paramsVersion = paramsVersion;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
