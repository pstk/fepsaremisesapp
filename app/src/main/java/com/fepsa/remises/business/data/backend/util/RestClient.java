package com.fepsa.remises.business.data.backend.util;

import com.fepsa.remises.BuildConfig;
import com.fepsa.remises.business.data.backend.ApiService;
import com.fepsa.remises.ui.util.BaseFunctions;
import com.fepsa.remises.util.AuthUtils;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created by tksko on 02/07/2017.
 */

public enum RestClient {

    INSTANCE;

    private ApiService apiService;

    private OkHttpClient okHttpClient = new OkHttpClient();

    public ApiService getApiService() {

        if (apiService == null) {

            synchronized (INSTANCE) {
                if (apiService == null)
                    init();
            }
        }

        return apiService;

    }

    private void init() {

        Retrofit restAdapter = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(configureHttpClient())
                .addConverterFactory(JacksonConverterFactory.create(BaseFunctions.getMapper()))
                .build();

        apiService = restAdapter.create(ApiService.class);

    }

    private OkHttpClient configureHttpClient() {

        configureUnsafeOkHttpClient();

        configureTimeouts();

        return okHttpClient;

    }

    private void configureUnsafeOkHttpClient() {

        try {
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws
                                CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws
                                CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            final SSLContext sslContext = SSLContext.getInstance("SSL");

            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            okHttpClient = okHttpClient.newBuilder()
                    .sslSocketFactory(sslSocketFactory)
                    .hostnameVerifier(new HostnameVerifier() {

                        @Override
                        public boolean verify(String hostname, SSLSession session) {
                            return true;
                        }

                    })
                    .build();

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    private void configureTimeouts() {

        okHttpClient = okHttpClient.newBuilder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {

                    @Override
                    public Response intercept(Chain chain) throws IOException {

                        Request original = chain.request();

                        Request.Builder builder = original
                                .newBuilder()
                                .header("Cache-Control", "no-cache");

                        if (AuthUtils.isLoggedIn()) {
                            builder.header("Authorization", "Bearer " + AuthUtils.getToken());
                        }

                        Request request = builder.build();

                        Response response = chain.proceed(request);

                        /*
                        if (response.body().contentLength() <= 0) {

                            //Obtengo el Content Type
                            MediaType contentType = response.body().contentType();

                            //Creo un BaseResponse, lo transformo a JSon y lo meto en un Body
                            ResponseBody body = ResponseBody.create(contentType, BaseFunctions.getMapper().writeValueAsString(new BaseResponse()));

                            //Piso el Response con este nuevo Body para que el Parse NO pinche
                            response = response.newBuilder().body(body).build();

                        }*/

                        return response;

                    }

                })
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();
    }

}
