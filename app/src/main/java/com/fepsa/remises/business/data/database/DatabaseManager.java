package com.fepsa.remises.business.data.database;

import com.fepsa.remises.business.data.database.model.RealmLocation;
import com.fepsa.remises.business.data.database.model.RealmPassenger;
import com.fepsa.remises.business.data.database.model.RealmPerson;
import com.fepsa.remises.business.data.database.model.RealmTrip;

import java.util.List;

import rx.Observable;

/**
 * Created by tksko on 02/07/2017.
 */

public class DatabaseManager {

    private Database database;

    public DatabaseManager() {

        this.database = RealmDatabase.getInstance();
    }

    public Observable<Boolean> clearDatabase() {

        return database.clearDatabase();
    }

    public Observable<Boolean> clearLocations() {

        return database.clearLocations();
    }

    public Observable<RealmLocation> saveLocation(RealmLocation location) {

        return database.saveLocation(location);
    }

    public Observable<List<RealmLocation>> getLocations() {

        return database.getLocations();
    }

    public Observable<RealmLocation> getLastLocation() {

        return database.getLastLocation();
    }

    public Observable<List<RealmLocation>> getTripLocations(long tripId) {

        return database.getTripLocations(tripId);
    }

    /// TRIPS

    public Observable<RealmTrip> saveTrip(RealmTrip newTrip, RealmLocation theLocation) {

        return database.saveTrip(newTrip, theLocation);
    }

    public Observable<Boolean> removeTrip(long tripId) {

        return database.removeTrip(tripId);
    }

    public Observable<RealmTrip> getActiveTrip() {

        return database.getActiveTrip();
    }

    public Observable<List<RealmTrip>> getPendingTrips() {

        return database.getPendingTrips();
    }

    // Passengers

    public Observable<RealmPassenger> addPassenger(long tripId, RealmPassenger passenger) {

        return database.addPassenger(tripId, passenger);
    }

    public Observable<RealmPassenger> removepassenger(long tripId, String personLegajo, RealmLocation location) {

        return database.removePassenger(tripId, personLegajo, location);
    }

    public Observable<List<RealmPassenger>> getPassengers(Long tripId) {

        return database.getPassengers(tripId);
    }

    // Persons

    public Observable<List<RealmPerson>> savePersons(List<RealmPerson> personsToSave) {

        return database.savePersons(personsToSave);
    }

    public Observable<List<RealmPerson>> getPersons(String query, int version) {

        return database.getPersons(query, version);
    }

    public Observable<Boolean> cleanPersons(int version) {

        return database.cleanPersons(version);
    }

}
