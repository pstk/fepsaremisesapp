package com.fepsa.remises.business.data.database.resolver;


import com.fepsa.remises.business.data.database.model.RealmLocation;

import java.util.List;

import io.realm.RealmResults;
import io.realm.Sort;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by tksko on 24/09/2017.
 */

public class GetLocationsDBResolver extends BaseDBResolver {

    public Observable<List<RealmLocation>> executeQuery() {

        return Observable.create(
                new Observable.OnSubscribe<List<RealmLocation>>() {

                    @Override
                    public void call(final Subscriber<? super List<RealmLocation>> subscriber) {

                        RealmResults<RealmLocation> clases = realm
                                .where(RealmLocation.class)
                                //    .equalTo("mProvider", "gps")
                                .sort("time", Sort.ASCENDING).findAll();

                        subscriber.onNext(clases);

                        subscriber.onCompleted();
                    }
                }
        )
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
