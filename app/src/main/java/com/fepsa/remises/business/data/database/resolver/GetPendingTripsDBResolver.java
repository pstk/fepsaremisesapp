package com.fepsa.remises.business.data.database.resolver;


import com.fepsa.remises.business.data.database.model.RealmTrip;

import java.util.List;

import io.realm.RealmResults;
import io.realm.Sort;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by tksko on 24/09/2017.
 */

public class GetPendingTripsDBResolver extends BaseDBResolver {

    public Observable<List<RealmTrip>> executeQuery() {

        return Observable.create(
                new Observable.OnSubscribe<List<RealmTrip>>() {

                    @Override
                    public void call(final Subscriber<? super List<RealmTrip>> subscriber) {

                        RealmResults<RealmTrip> pendingTrips = realm
                                .where(RealmTrip.class)
                                .isNotNull("endDate").sort("endDate", Sort.DESCENDING).findAll();

                        subscriber.onNext(pendingTrips);

                        subscriber.onCompleted();
                    }

                }

        )
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
