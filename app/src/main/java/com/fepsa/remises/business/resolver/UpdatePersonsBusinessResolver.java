package com.fepsa.remises.business.resolver;

import com.fepsa.remises.business.data.backend.BackendManager;
import com.fepsa.remises.business.data.backend.response.UpdatePersonasResponse;
import com.fepsa.remises.business.data.database.DatabaseManager;
import com.fepsa.remises.business.data.database.model.RealmPerson;
import com.fepsa.remises.util.SharedPrefs;

import java.util.List;

import rx.Subscriber;

/**
 * Created by tksko on 02/07/2017.
 */

public class UpdatePersonsBusinessResolver extends BaseBusinessResolver<Boolean, UpdatePersonasResponse> {

    public UpdatePersonsBusinessResolver(DatabaseManager databaseManager, BackendManager backendManager) {
        super(databaseManager, backendManager);
    }

    @Override
    protected void getObservableActionsFlow() {

        if (isNetworkConnectivityAvailable()) {

            callBackend();
        } else {

            subscriber.onError(new Exception("No hay conexión a internet."));
        }

    }

    @Override
    protected void callBackend() {

        backendManager.updatePersons().subscribe(this);

    }

    @Override
    protected void onSuccessCallActions(final UpdatePersonasResponse response) {

        databaseManager.savePersons(RealmPerson.fromEntityList(response.getPersonas(), response.getVersion())).subscribe(new Subscriber<List<RealmPerson>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

                onFailureCallActions(e);

            }

            @Override
            public void onNext(List<RealmPerson> realmPersons) {

                SharedPrefs.saveInt(SharedPrefs.Constants.PERSONS_DB_VERSION, response.getVersion());

                subscriber.onNext(true);

                subscriber.onCompleted();

            }

        });

    }

}
