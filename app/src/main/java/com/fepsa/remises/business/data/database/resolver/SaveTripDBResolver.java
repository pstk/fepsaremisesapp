package com.fepsa.remises.business.data.database.resolver;


import com.fepsa.remises.business.data.database.model.RealmLocation;
import com.fepsa.remises.business.data.database.model.RealmPassenger;
import com.fepsa.remises.business.data.database.model.RealmTrip;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by tksko on 24/09/2017.
 */

public class SaveTripDBResolver extends BaseDBResolver {

    public Observable<RealmTrip> executeQuery(final RealmTrip trip, final RealmLocation newLocation) {

        return Observable.create(
                new Observable.OnSubscribe<RealmTrip>() {

                    @Override
                    public void call(final Subscriber<? super RealmTrip> subscriber) {

                        try {

                            realm.beginTransaction();

                            RealmTrip theTrip = realm.where(RealmTrip.class).equalTo("startDate", trip.getStartDate()).findFirst();

                            // Alta de viaje!
                            if (theTrip == null) {

                                theTrip = realm.copyToRealmOrUpdate(trip);

                            } else {

                                // Cerrando un viaje
                                theTrip.setNroViaje(trip.getNroViaje());
                                theTrip.setPeajes(trip.getPeajes());
                                theTrip.setMinutes(trip.getMinutes());
                                theTrip.setEndDate(trip.getEndDate());
                                theTrip.setTicket(trip.getTicket());

                            }

                            RealmLocation theLocation = theTrip.getLocations().where().equalTo("time", newLocation.getTime()).equalTo("tripId", trip.getStartDate()).findFirst();

                            if (theLocation == null) {

                                theLocation = realm.copyToRealm(newLocation);

                                theTrip.getLocations().add(theLocation);

                            }

                            // Todos los pasajeros que no bajaron durante el viaje, se bajan aca
                            for (RealmPassenger passenger : theTrip.getPassengers()) {

                                if (passenger.getPosBaja() == null) {
                                    passenger.setPosBaja(theLocation);
                                }

                            }

                            realm.commitTransaction();

                            subscriber.onNext(theTrip);

                            subscriber.onCompleted();

                        } catch (Exception e) {

                            realm.cancelTransaction();

                            subscriber.onError(e);
                        }

                    }

                }
        )
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
