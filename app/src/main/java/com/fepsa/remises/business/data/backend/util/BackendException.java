package com.fepsa.remises.business.data.backend.util;

/**
 * Created by tksko on 02/07/2017.
 */

public class BackendException extends Exception {

    public BackendException(String error) {
        super(error);
    }

    public BackendException() {
        super();
    }

}
