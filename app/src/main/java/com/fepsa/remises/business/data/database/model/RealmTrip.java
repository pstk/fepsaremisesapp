package com.fepsa.remises.business.data.database.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by nicolas.m on 11/3/2017.
 */

public class RealmTrip extends RealmObject {

    @PrimaryKey
    private Long startDate;

    private Long endDate;

    private RealmList<RealmPassenger> passengers;

    private RealmList<RealmLocation> locations;

    private int minutes;

    private double peajes;

    private long nroViaje;

    private double ticket;

    public RealmTrip() {

        this.startDate = null;
        this.endDate = null;
        this.passengers = new RealmList<>();
        this.locations = new RealmList<>();
        this.minutes = 0;
        this.peajes = 0;
        this.nroViaje = 0;
        this.ticket = 0;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    public RealmList<RealmPassenger> getPassengers() {
        return passengers;
    }

    public void setPassengers(RealmList<RealmPassenger> passengers) {
        this.passengers = passengers;
    }

    public RealmList<RealmLocation> getLocations() {
        return locations;
    }

    public void setLocations(RealmList<RealmLocation> locations) {
        this.locations = locations;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public double getPeajes() {
        return peajes;
    }

    public void setPeajes(double peajes) {
        this.peajes = peajes;
    }

    public long getNroViaje() {
        return nroViaje;
    }

    public void setNroViaje(long nroViaje) {
        this.nroViaje = nroViaje;
    }

    public double getTicket() {
        return ticket;
    }

    public void setTicket(double ticket) {
        this.ticket = ticket;
    }
}
