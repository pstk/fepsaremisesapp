package com.fepsa.remises.business.data.backend.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fepsa.remises.business.data.backend.model.PersonEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nicolas.m on 12/4/2017.
 */

@JsonIgnoreProperties(
        ignoreUnknown = true
)
public class UpdatePersonasResponse extends BaseResponse {

    @JsonProperty("pasajeros")
    private List<PersonEntity> personas;

    @JsonProperty("version")
    private int version;

    public UpdatePersonasResponse() {
        personas = new ArrayList<>();
        version = 0;
    }

    public List<PersonEntity> getPersonas() {
        return personas;
    }

    public void setPersonas(List<PersonEntity> personas) {
        this.personas = personas;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}
