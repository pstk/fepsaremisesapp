package com.fepsa.remises.business.data.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fepsa.remises.business.data.database.model.RealmLocation;
import com.fepsa.remises.ui.util.BaseFunctions;

/**
 * Created by nicolas.m on 12/18/2017.
 */

@JsonIgnoreProperties(
        ignoreUnknown = true
)
public class PosicionEntity {

    private long id;

    private double latitud;

    private double longitud;

    private float velocidad;

    private float rumbo;

    private float accuracy;

    private String fecha;

    private int versionLogica;

    public PosicionEntity(RealmLocation theLocation) {
        this.id = theLocation.getTime();
        this.latitud = theLocation.getLatitude();
        this.longitud = theLocation.getLongitude();
        this.velocidad = theLocation.getSpeed();
        this.rumbo = theLocation.getBearing();
        this.accuracy = theLocation.getAccuracy();
        this.fecha = BaseFunctions.formatDateISO(theLocation.getTime());
        this.versionLogica = theLocation.getParamsVersion();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public float getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(float velocidad) {
        this.velocidad = velocidad;
    }

    public float getRumbo() {
        return rumbo;
    }

    public void setRumbo(float rumbo) {
        this.rumbo = rumbo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getVersionLogica() {
        return versionLogica;
    }

    public void setVersionLogica(int versionLogica) {
        this.versionLogica = versionLogica;
    }

    public float getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(float accuracy) {
        this.accuracy = accuracy;
    }
}
