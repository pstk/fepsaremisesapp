package com.fepsa.remises.business.data.database;

import com.fepsa.remises.business.data.database.model.RealmLocation;
import com.fepsa.remises.business.data.database.model.RealmPassenger;
import com.fepsa.remises.business.data.database.model.RealmPerson;
import com.fepsa.remises.business.data.database.model.RealmTrip;
import com.fepsa.remises.business.data.database.resolver.CleanPersonsDBResolver;
import com.fepsa.remises.business.data.database.resolver.ClearDatabaseDBResolver;
import com.fepsa.remises.business.data.database.resolver.ClearLocationDBResolver;
import com.fepsa.remises.business.data.database.resolver.GetActiveTripDBResolver;
import com.fepsa.remises.business.data.database.resolver.GetLatestLocationDBResolver;
import com.fepsa.remises.business.data.database.resolver.GetLocationsDBResolver;
import com.fepsa.remises.business.data.database.resolver.GetPassengersDBResolver;
import com.fepsa.remises.business.data.database.resolver.GetPendingTripsDBResolver;
import com.fepsa.remises.business.data.database.resolver.GetPersonsByQueryDBResolver;
import com.fepsa.remises.business.data.database.resolver.GetTripLocationsDBResolver;
import com.fepsa.remises.business.data.database.resolver.RemovePassengerDBResolver;
import com.fepsa.remises.business.data.database.resolver.RemoveTripDBResolver;
import com.fepsa.remises.business.data.database.resolver.SaveLocationDBResolver;
import com.fepsa.remises.business.data.database.resolver.SavePassengerDBResolver;
import com.fepsa.remises.business.data.database.resolver.SaveTripDBResolver;
import com.fepsa.remises.business.data.database.resolver.UpdatePersonsDBResolver;
import com.fepsa.remises.ui.application.App;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import rx.Observable;

/**
 * Created by tksko on 02/07/2017.
 */

public class RealmDatabase implements Database {

    private static RealmDatabase INSTANCE;

    private static final int DB_VERSION = 1;

    private Realm realm;

    public static RealmDatabase getInstance() {

        if (INSTANCE == null) {

            INSTANCE = new RealmDatabase();
        }

        return INSTANCE;
    }

    public RealmDatabase() {

        Realm.init(App.applicationContext);

        RealmConfiguration config = new RealmConfiguration.Builder()
                .schemaVersion(DB_VERSION) // Migrate to the new version(if required).
                .deleteRealmIfMigrationNeeded()
                .build();

        Realm.setDefaultConfiguration(config);

        realm = Realm.getDefaultInstance();

    }

    public Realm getRealm() {
        return realm;
    }

    @Override
    public Observable<Boolean> clearDatabase() {

        ClearDatabaseDBResolver resolver = new ClearDatabaseDBResolver();

        return resolver.executeQuery();
    }

    @Override
    public Observable<Boolean> clearLocations() {

        ClearLocationDBResolver resolver = new ClearLocationDBResolver();

        return resolver.executeQuery();
    }

    @Override
    public Observable<RealmLocation> saveLocation(RealmLocation location) {

        SaveLocationDBResolver resolver = new SaveLocationDBResolver();

        return resolver.executeQuery(location);
    }

    @Override
    public Observable<List<RealmLocation>> getLocations() {

        GetLocationsDBResolver resolver = new GetLocationsDBResolver();

        return resolver.executeQuery();
    }

    @Override
    public Observable<RealmLocation> getLastLocation() {

        GetLatestLocationDBResolver resolver = new GetLatestLocationDBResolver();

        return resolver.executeQuery();
    }

    @Override
    public Observable<List<RealmLocation>> getTripLocations(long tripId) {

        GetTripLocationsDBResolver resolver = new GetTripLocationsDBResolver();

        return resolver.executeQuery(tripId);
    }

    /// TRIPS

    @Override
    public Observable<RealmTrip> saveTrip(RealmTrip newTrip, RealmLocation location) {

        SaveTripDBResolver resolver = new SaveTripDBResolver();

        return resolver.executeQuery(newTrip, location);
    }

    @Override
    public Observable<Boolean> removeTrip(long tripId) {

        RemoveTripDBResolver resolver = new RemoveTripDBResolver();

        return resolver.executeQuery(tripId);
    }

    @Override
    public Observable<RealmTrip> getActiveTrip() {

        GetActiveTripDBResolver resolver = new GetActiveTripDBResolver();

        return resolver.executeQuery();
    }

    @Override
    public Observable<List<RealmTrip>> getPendingTrips() {

        GetPendingTripsDBResolver resolver = new GetPendingTripsDBResolver();

        return resolver.executeQuery();
    }

    // Passengers

    @Override
    public Observable<RealmPassenger> addPassenger(long tripId, RealmPassenger passenger) {

        SavePassengerDBResolver resolver = new SavePassengerDBResolver();

        return resolver.executeQuery(tripId, passenger);
    }

    @Override
    public Observable<RealmPassenger> removePassenger(long tripId, String personLegajo, RealmLocation location) {

        RemovePassengerDBResolver resolver = new RemovePassengerDBResolver();

        return resolver.executeQuery(tripId, personLegajo, location);
    }

    @Override
    public Observable<List<RealmPassenger>> getPassengers(Long tripId) {

        GetPassengersDBResolver resolver = new GetPassengersDBResolver();

        return resolver.executeQuery(tripId);
    }

    // Persons

    @Override
    public Observable<List<RealmPerson>> savePersons(List<RealmPerson> personsToSave) {

        UpdatePersonsDBResolver resolver = new UpdatePersonsDBResolver();

        return resolver.executeQuery(personsToSave);
    }

    @Override
    public Observable<List<RealmPerson>> getPersons(String query, int version) {

        GetPersonsByQueryDBResolver resolver = new GetPersonsByQueryDBResolver();

        return resolver.executeQuery(query, version);
    }

    @Override
    public Observable<Boolean> cleanPersons(int version) {

        CleanPersonsDBResolver resolver = new CleanPersonsDBResolver();

        return resolver.executeQuery(version);
    }

}