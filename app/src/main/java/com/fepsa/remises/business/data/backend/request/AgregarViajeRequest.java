package com.fepsa.remises.business.data.backend.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fepsa.remises.business.data.backend.model.PasajeroEntity;
import com.fepsa.remises.business.data.backend.model.PosicionEntity;
import com.fepsa.remises.business.data.database.model.RealmLocation;
import com.fepsa.remises.business.data.database.model.RealmPassenger;
import com.fepsa.remises.business.data.database.model.RealmTrip;
import com.fepsa.remises.ui.util.BaseFunctions;
import com.fepsa.remises.util.AuthUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nicolas.m on 12/18/2017.
 */

@JsonIgnoreProperties(
        ignoreUnknown = true
)
public class AgregarViajeRequest {

    @JsonProperty("fechaInicio")
    private String fechaInicio;

    @JsonProperty("fechaFin")
    private String fechaFin;

    @JsonProperty("choferId")
    private long choferId;

    @JsonProperty("tiempoEspera")
    private int tiempoEspera;

    @JsonProperty("peajes")
    private double peajes;

    @JsonProperty("numeroViaje")
    private long numeroViaje;

    @JsonProperty("posiciones")
    private List<PosicionEntity> posiciones;

    @JsonProperty("pasajeros")
    private List<PasajeroEntity> pasajeros;

    @JsonProperty("ticket")
    private double ticket;

    public AgregarViajeRequest(RealmTrip theTrip) {

        this.fechaInicio = BaseFunctions.formatDateISO(theTrip.getStartDate());
        this.fechaFin = BaseFunctions.formatDateISO(theTrip.getEndDate());
        this.choferId = Long.parseLong(AuthUtils.getChoferID());
        this.tiempoEspera = theTrip.getMinutes();
        this.peajes = theTrip.getPeajes();
        this.numeroViaje = theTrip.getNroViaje();
        this.posiciones = new ArrayList<>();
        this.pasajeros = new ArrayList<>();

        for (RealmLocation location : theTrip.getLocations()) {
            this.posiciones.add(new PosicionEntity(location));
        }

        for (RealmPassenger passenger : theTrip.getPassengers()) {
            this.pasajeros.add(new PasajeroEntity(passenger));
        }

        this.ticket = theTrip.getTicket();

    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public long getChoferId() {
        return choferId;
    }

    public void setChoferId(long choferId) {
        this.choferId = choferId;
    }

    public int getTiempoEspera() {
        return tiempoEspera;
    }

    public void setTiempoEspera(int tiempoEspera) {
        this.tiempoEspera = tiempoEspera;
    }

    public double getPeajes() {
        return peajes;
    }

    public void setPeajes(double peajes) {
        this.peajes = peajes;
    }

    public long getNumeroViaje() {
        return numeroViaje;
    }

    public void setNumeroViaje(long numeroViaje) {
        this.numeroViaje = numeroViaje;
    }

    public List<PosicionEntity> getPosiciones() {
        return posiciones;
    }

    public void setPosiciones(List<PosicionEntity> posiciones) {
        this.posiciones = posiciones;
    }

    public List<PasajeroEntity> getPasajeros() {
        return pasajeros;
    }

    public void setPasajeros(List<PasajeroEntity> pasajeros) {
        this.pasajeros = pasajeros;
    }

    public void addPosicion(PosicionEntity pos) {

        this.posiciones.add(pos);
    }

    public void addPasajero(PasajeroEntity pas) {

        this.pasajeros.add(pas);
    }

}
