package com.fepsa.remises.business.data.database.resolver;


import com.fepsa.remises.business.data.database.model.RealmPerson;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by tksko on 11/04/2019.
 */

public class CleanPersonsDBResolver extends BaseDBResolver {

    public Observable<Boolean> executeQuery(final int version) {

        return Observable.create(
                new Observable.OnSubscribe<Boolean>() {

                    @Override
                    public void call(final Subscriber<? super Boolean> subscriber) {

                        realm.beginTransaction();

                        realm.where(RealmPerson.class).notEqualTo("version", version).findAll().deleteAllFromRealm();

                        realm.commitTransaction();

                        subscriber.onNext(true);

                        subscriber.onCompleted();

                    }
                }
        )
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
