package com.fepsa.remises.business.services;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.fepsa.remises.business.receivers.NewLocationReceiver;
import com.fepsa.remises.config.AppConstants;
import com.fepsa.remises.ui.util.NotificationUtil;
import com.fepsa.remises.util.ParametrosUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/**
 * Created by tksko on 30/9/2017.
 */

public class FusedLocationService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private final String TAG = "FUSED_LOCATION";

    private GoogleApiClient mGoogleAPIClient;
    private LocationRequest mLocationRequest;
    private FusedLocationProviderApi mLocationProvider;

    private NewLocationReceiver newLocationReceiver = null;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.d(TAG, "onCreate");

        registerNewLocationReceiver();

        startForeground(NotificationUtil.RUNNING_NOTIFICATION_ID, NotificationUtil.getNotification(this));

        mGoogleAPIClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mLocationProvider = LocationServices.FusedLocationApi;

        mLocationRequest = new LocationRequest();

        mLocationRequest.setInterval(ParametrosUtils.getInstance().getGPSInterval());

        //  mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        //NotificationUtil.showRunningNotification(this);
        if (mGoogleAPIClient != null) {
            mGoogleAPIClient.connect();
        }
        return START_STICKY;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        RequestLocationUpdates();
    }

    private void RequestLocationUpdates() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        Log.d(TAG, "requestLocationUpdates");

        mLocationProvider.requestLocationUpdates(mGoogleAPIClient, mLocationRequest, this);

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e(TAG, "onConnectionSuspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed");
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "::::***********Latitude: " + location.getLatitude() + " Longitude: " + location.getLongitude());
        sendNewLocationBroadcast(location);
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        if (newLocationReceiver != null)
            unregisterReceiver(newLocationReceiver);
        NotificationUtil.hideRunningNotification(this);
        if (mGoogleAPIClient != null) {
            mGoogleAPIClient.disconnect();
        }
        super.onDestroy();
    }

    private void sendNewLocationBroadcast(Location theLocation) {

        Intent newLocationIntent = new Intent(AppConstants.BroadcastActions.NEW_LOCATION);

        newLocationIntent.putExtra("NEW_LOCATION", theLocation);

        sendBroadcast(newLocationIntent);

    }

    private void registerNewLocationReceiver() {

        // Create an IntentFilter instance.
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("FEPSA_NEW_LOCATION");

        newLocationReceiver = new NewLocationReceiver();

        registerReceiver(newLocationReceiver, intentFilter);

    }

}
