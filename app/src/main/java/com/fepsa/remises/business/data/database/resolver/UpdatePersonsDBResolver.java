package com.fepsa.remises.business.data.database.resolver;


import com.fepsa.remises.business.data.database.model.RealmPerson;

import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by tksko on 24/09/2017.
 */

public class UpdatePersonsDBResolver extends BaseDBResolver {

    public Observable<List<RealmPerson>> executeQuery(final List<RealmPerson> persons) {

        return Observable.create(
                new Observable.OnSubscribe<List<RealmPerson>>() {

                    @Override
                    public void call(final Subscriber<? super List<RealmPerson>> subscriber) {

                        realm.beginTransaction();

                        List<RealmPerson> newPersons = realm.copyToRealmOrUpdate(persons);

                        realm.commitTransaction();

                        subscriber.onNext(newPersons);

                        subscriber.onCompleted();

                    }

                }
        )
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
