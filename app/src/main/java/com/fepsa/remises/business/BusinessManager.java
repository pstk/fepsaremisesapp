package com.fepsa.remises.business;

import com.fepsa.remises.business.data.backend.BackendManager;
import com.fepsa.remises.business.data.database.DatabaseManager;
import com.fepsa.remises.business.resolver.LoginChoferBusinessResolver;
import com.fepsa.remises.business.resolver.SearchPersonsBusinessResolver;
import com.fepsa.remises.business.resolver.UpdatePersonsBusinessResolver;
import com.fepsa.remises.business.resolver.UploadTripBusinessResolver;
import com.fepsa.remises.ui.model.PersonDTO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;

/**
 * Created by tksko on 02/07/2017.
 */
public class BusinessManager {

    private BackendManager backendManager;

    private DatabaseManager databaseManager;

    public BusinessManager() {

        this.backendManager = new BackendManager();

        this.databaseManager = new DatabaseManager();
    }

    // Login Choferes

    public Observable<Boolean> loginChofer(String nombre, String clave, String dni) {

        LoginChoferBusinessResolver coreResolver = new LoginChoferBusinessResolver(backendManager);

        Map<String, Object> params = new HashMap<>();
        params.put("nombre", nombre);
        params.put("clave", clave);
        params.put("dni", dni);

        return coreResolver.getObservable(params);

    }

    // Persons

    public Observable<List<PersonDTO>> searchPersons(String query, boolean forceBackend) {

        SearchPersonsBusinessResolver coreResolver = new SearchPersonsBusinessResolver(databaseManager, backendManager);
        coreResolver.setForceBackend(forceBackend);

        Map<String, Object> params = new HashMap<>();
        params.put("query", query);

        return coreResolver.getObservable(params);

    }

    public Observable<Boolean> updatePersons() {

        UpdatePersonsBusinessResolver coreResolver = new UpdatePersonsBusinessResolver(databaseManager, backendManager);

        return coreResolver.getObservable(null);
    }

    // Upload Trips

    public Observable<Boolean> uploadTrips() {

        UploadTripBusinessResolver coreResolver = new UploadTripBusinessResolver(databaseManager, backendManager);

        return coreResolver.getObservable(null);

    }

    public BackendManager getBackendManager() {
        return backendManager;
    }

    public DatabaseManager getDatabaseManager() {
        return databaseManager;
    }

}
