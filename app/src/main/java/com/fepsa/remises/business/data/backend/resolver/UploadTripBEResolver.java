package com.fepsa.remises.business.data.backend.resolver;

import com.fepsa.remises.business.data.backend.response.BaseResponse;

import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * Created by tksko on 12/18/2017.
 */

public class UploadTripBEResolver extends BaseBEResolver<BaseResponse> {

    @Override
    protected void getDataFromBackend(Map<String, Object> params) {

        try {

            RequestBody requestBody = RequestBody.create(MediaType.parse(
                    "application/json; charset=utf-8"),
                    String.valueOf(params.get("jsonRequest")).getBytes("UTF-8")
            );

            client.uploadTrip(requestBody).enqueue(this);

        } catch (Exception e) {

            subscriber.onError(e);

        }

    }

}
