package com.fepsa.remises.business.data.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by tksko on 11/11/2017.
 */

@JsonIgnoreProperties(
        ignoreUnknown = true
)
public class ParametersEntity {

    @JsonProperty("controlRumbo")
    private boolean controlRumbo;

    @JsonProperty("controlTiempoVelocidad")
    private boolean controlTiempoVelocidad;

    @JsonProperty("controlDistanciaVelocidad")
    private boolean controlDistanciaVelocidad;

    @JsonProperty("velocidad1")
    private float velocidad1;

    @JsonProperty("velocidad2")
    private float velocidad2;

    @JsonProperty("velocidad3")
    private float velocidad3;

    @JsonProperty("velocidad4")
    private float velocidad4;

    @JsonProperty("tiempo1")
    private long tiempo1;

    @JsonProperty("tiempo2")
    private long tiempo2;

    @JsonProperty("tiempo3")
    private long tiempo3;

    @JsonProperty("distancia1")
    private float distancia1;

    @JsonProperty("distancia2")
    private float distancia2;

    @JsonProperty("distancia3")
    private float distancia3;

    @JsonProperty("rumboGrados")
    private float rumboGrados;

    @JsonProperty("rumboDistancia")
    private float rumboDistancia;

    @JsonProperty("versionPasajeros")
    private int versionPersonas;

    @JsonProperty("versionParametros")
    private int versionParametros;

    @JsonProperty("gpsInterval")
    private int gpsInterval;

    public ParametersEntity() {

        this.controlRumbo = false;
        this.controlTiempoVelocidad = false;
        this.controlDistanciaVelocidad = false;
        this.velocidad1 = 0;
        this.velocidad2 = 0;
        this.velocidad3 = 0;
        this.velocidad4 = 0;
        this.tiempo1 = 0;
        this.tiempo2 = 0;
        this.tiempo3 = 0;
        this.distancia1 = 0;
        this.distancia2 = 0;
        this.distancia3 = 0;
        this.rumboGrados = 0;
        this.rumboDistancia = 0;

        this.versionParametros = 0;
        this.versionPersonas = 0;

        this.gpsInterval = 0;

    }

    public boolean isControlRumbo() {
        return controlRumbo;
    }

    public void setControlRumbo(boolean controlRumbo) {
        this.controlRumbo = controlRumbo;
    }

    public boolean isControlTiempoVelocidad() {
        return controlTiempoVelocidad;
    }

    public void setControlTiempoVelocidad(boolean controlTiempoVelocidad) {
        this.controlTiempoVelocidad = controlTiempoVelocidad;
    }

    public boolean isControlDistanciaVelocidad() {
        return controlDistanciaVelocidad;
    }

    public void setControlDistanciaVelocidad(boolean controlDistanciaVelocidad) {
        this.controlDistanciaVelocidad = controlDistanciaVelocidad;
    }

    public float getVelocidad1() {
        return velocidad1;
    }

    public void setVelocidad1(float velocidad1) {
        this.velocidad1 = velocidad1;
    }

    public float getVelocidad2() {
        return velocidad2;
    }

    public void setVelocidad2(float velocidad2) {
        this.velocidad2 = velocidad2;
    }

    public float getVelocidad3() {
        return velocidad3;
    }

    public void setVelocidad3(float velocidad3) {
        this.velocidad3 = velocidad3;
    }

    public float getVelocidad4() {
        return velocidad4;
    }

    public void setVelocidad4(float velocidad4) {
        this.velocidad4 = velocidad4;
    }

    public long getTiempo1() {
        return tiempo1;
    }

    public void setTiempo1(long tiempo1) {
        this.tiempo1 = tiempo1;
    }

    public long getTiempo2() {
        return tiempo2;
    }

    public void setTiempo2(long tiempo2) {
        this.tiempo2 = tiempo2;
    }

    public long getTiempo3() {
        return tiempo3;
    }

    public void setTiempo3(long tiempo3) {
        this.tiempo3 = tiempo3;
    }

    public float getDistancia1() {
        return distancia1;
    }

    public void setDistancia1(float distancia1) {
        this.distancia1 = distancia1;
    }

    public float getDistancia2() {
        return distancia2;
    }

    public void setDistancia2(float distancia2) {
        this.distancia2 = distancia2;
    }

    public float getDistancia3() {
        return distancia3;
    }

    public void setDistancia3(float distancia3) {
        this.distancia3 = distancia3;
    }

    public float getRumboGrados() {
        return rumboGrados;
    }

    public void setRumboGrados(float rumboGrados) {
        this.rumboGrados = rumboGrados;
    }

    public float getRumboDistancia() {
        return rumboDistancia;
    }

    public void setRumboDistancia(float rumboDistancia) {
        this.rumboDistancia = rumboDistancia;
    }

    public int getVersionPersonas() {
        return versionPersonas;
    }

    public void setVersionPersonas(int versionPersonas) {
        this.versionPersonas = versionPersonas;
    }

    public int getVersionParametros() {
        return versionParametros;
    }

    public void setVersionParametros(int versionParametros) {
        this.versionParametros = versionParametros;
    }

    public int getGpsInterval() {
        return gpsInterval;
    }

    public void setGpsInterval(int gpsInterval) {
        this.gpsInterval = gpsInterval;
    }

}
