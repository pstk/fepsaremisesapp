package com.fepsa.remises.business.model;

import com.fepsa.remises.business.data.backend.model.ParametersEntity;

/**
 * Created by tksko on 11/11/2017.
 */

public class ParametersModel {

    private boolean controlRumbo;

    private boolean controlTiempoVelocidad;

    private boolean controlDistanciaVelocidad;

    private float velocidad1;
    private float velocidad2;
    private float velocidad3;
    private float velocidad4;

    private long tiempo1;
    private long tiempo2;
    private long tiempo3;

    private float distancia1;
    private float distancia2;
    private float distancia3;

    private float rumboGrados;
    private float rumboDistancia;

    private int versionPersonas;
    private int versionParametros;

    private int gpsInterval;

    public ParametersModel() {

        this.controlRumbo = true;
        this.controlTiempoVelocidad = true;
        this.controlDistanciaVelocidad = true;
        this.velocidad1 = 1.4f;
        this.velocidad2 = 30.5f;
        this.velocidad3 = 1.4f;
        this.velocidad4 = 30.5f;
        this.tiempo1 = 600;
        this.tiempo2 = 120;
        this.tiempo3 = 3;
        this.distancia1 = 100;
        this.distancia2 = 3000;
        this.distancia3 = 3000;
        this.rumboGrados = 30;
        this.rumboDistancia = 70;

        this.versionParametros = 0;
        this.versionPersonas = 0;

        this.gpsInterval = 3000; // miliseconds

    }

    public ParametersModel(ParametersEntity entity) {

        this.controlRumbo = entity.isControlRumbo();
        this.controlTiempoVelocidad = entity.isControlTiempoVelocidad();
        this.controlDistanciaVelocidad = entity.isControlDistanciaVelocidad();
        this.velocidad1 = entity.getVelocidad1();
        this.velocidad2 = entity.getVelocidad2();
        this.velocidad3 = entity.getVelocidad3();
        this.velocidad4 = entity.getVelocidad4();
        this.tiempo1 = entity.getTiempo1();
        this.tiempo2 = entity.getTiempo2();
        this.tiempo3 = entity.getTiempo3();
        this.distancia1 = entity.getDistancia1();
        this.distancia2 = entity.getDistancia2();
        this.distancia3 = entity.getDistancia3();
        this.rumboGrados = entity.getRumboGrados();
        this.rumboDistancia = entity.getRumboDistancia();

        this.versionParametros = entity.getVersionParametros();
        this.versionPersonas = entity.getVersionPersonas();

        this.gpsInterval = entity.getGpsInterval();

        // Not in miliseconds...
        if (this.gpsInterval % 1000 != 0) {
            this.gpsInterval *= 1000;
        }

    }

    public boolean isControlRumbo() {
        return controlRumbo;
    }

    public void setControlRumbo(boolean controlRumbo) {
        this.controlRumbo = controlRumbo;
    }

    public boolean isControlTiempoVelocidad() {
        return controlTiempoVelocidad;
    }

    public void setControlTiempoVelocidad(boolean controlTiempoVelocidad) {
        this.controlTiempoVelocidad = controlTiempoVelocidad;
    }

    public boolean isControlDistanciaVelocidad() {
        return controlDistanciaVelocidad;
    }

    public void setControlDistanciaVelocidad(boolean controlDistanciaVelocidad) {
        this.controlDistanciaVelocidad = controlDistanciaVelocidad;
    }

    public float getVelocidad1() {
        return velocidad1;
    }

    public void setVelocidad1(float velocidad1) {
        this.velocidad1 = velocidad1;
    }

    public float getVelocidad2() {
        return velocidad2;
    }

    public void setVelocidad2(float velocidad2) {
        this.velocidad2 = velocidad2;
    }

    public float getVelocidad3() {
        return velocidad3;
    }

    public void setVelocidad3(float velocidad3) {
        this.velocidad3 = velocidad3;
    }

    public float getVelocidad4() {
        return velocidad4;
    }

    public void setVelocidad4(float velocidad4) {
        this.velocidad4 = velocidad4;
    }

    public long getTiempo1() {
        return tiempo1;
    }

    public void setTiempo1(long tiempo1) {
        this.tiempo1 = tiempo1;
    }

    public long getTiempo2() {
        return tiempo2;
    }

    public void setTiempo2(long tiempo2) {
        this.tiempo2 = tiempo2;
    }

    public long getTiempo3() {
        return tiempo3;
    }

    public void setTiempo3(long tiempo3) {
        this.tiempo3 = tiempo3;
    }

    public float getDistancia1() {
        return distancia1;
    }

    public void setDistancia1(float distancia1) {
        this.distancia1 = distancia1;
    }

    public float getDistancia2() {
        return distancia2;
    }

    public void setDistancia2(float distancia2) {
        this.distancia2 = distancia2;
    }

    public float getDistancia3() {
        return distancia3;
    }

    public void setDistancia3(float distancia3) {
        this.distancia3 = distancia3;
    }

    public float getRumboGrados() {
        return rumboGrados;
    }

    public void setRumboGrados(float rumboGrados) {
        this.rumboGrados = rumboGrados;
    }

    public float getRumboDistancia() {
        return rumboDistancia;
    }

    public void setRumboDistancia(float rumboDistancia) {
        this.rumboDistancia = rumboDistancia;
    }

    public int getVersionPersonas() {
        return versionPersonas;
    }

    public void setVersionPersonas(int versionPersonas) {
        this.versionPersonas = versionPersonas;
    }

    public int getVersionParametros() {
        return versionParametros;
    }

    public void setVersionParametros(int versionParametros) {
        this.versionParametros = versionParametros;
    }

    public int getGpsInterval() {
        return gpsInterval;
    }

    public void setGpsInterval(int gpsInterval) {
        this.gpsInterval = gpsInterval;
    }
}
