package com.fepsa.remises.business.data.database.resolver;

import com.fepsa.remises.business.data.database.model.RealmLocation;
import com.fepsa.remises.business.data.database.model.RealmTrip;

import io.realm.Sort;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by tksko on 24/09/2017.
 */

public class GetLatestLocationDBResolver extends BaseDBResolver {

    public Observable<RealmLocation> executeQuery() {

        return Observable.create(
                new Observable.OnSubscribe<RealmLocation>() {

                    @Override
                    public void call(final Subscriber<? super RealmLocation> subscriber) {

                        // Active trip
                        RealmTrip theTrip = realm
                                .where(RealmTrip.class)
                                .isNull("endDate").findFirst();

                        if (theTrip != null) {

                            //long latestDate = SharedPrefs.fetchLong(SharedPrefs.Constants.LATEST_SAVED_DATE, 0);

                            RealmLocation location = theTrip.getLocations()
                                    .where()
                                    .sort("time", Sort.DESCENDING)
                                    .findFirst();

                            subscriber.onNext(location);

                            subscriber.onCompleted();

                        } else {

                            subscriber.onError(new Exception("No hay un viaje en curso."));

                        }

                    }

                }

        )
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
