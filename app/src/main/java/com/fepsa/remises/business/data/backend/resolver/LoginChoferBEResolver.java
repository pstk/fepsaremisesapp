package com.fepsa.remises.business.data.backend.resolver;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fepsa.remises.business.data.backend.model.LoginEntity;
import com.fepsa.remises.business.data.backend.request.LoginChoferRequest;
import com.fepsa.remises.ui.util.BaseFunctions;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * Created by tksko on 02/07/2017.
 */

public class LoginChoferBEResolver extends BaseBEResolver<LoginEntity> {

    @Override
    protected void getDataFromBackend(Map<String, Object> params) {

        try {

            String json = BaseFunctions.getMapper().writeValueAsString(
                    new LoginChoferRequest(
                            String.valueOf(params.get("nombre")),
                            String.valueOf(params.get("clave")),
                            String.valueOf(params.get("dni"))
                    )
            );

            RequestBody requestBody = RequestBody.create(MediaType.parse(
                    "application/json; charset=utf-8"), json.getBytes("UTF-8")
            );

            client.loginChofer(
                    requestBody
            ).enqueue(this);

        } catch (JsonProcessingException | UnsupportedEncodingException e) {

            subscriber.onError(e);

        }

    }

}
