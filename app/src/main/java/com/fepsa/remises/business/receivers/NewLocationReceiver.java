package com.fepsa.remises.business.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.util.Log;

import com.fepsa.remises.business.data.database.model.RealmLocation;
import com.fepsa.remises.business.model.ParametersModel;
import com.fepsa.remises.ui.application.App;
import com.fepsa.remises.ui.util.BaseFunctions;
import com.fepsa.remises.util.LocationUtils;
import com.fepsa.remises.util.ParametrosUtils;

import rx.Subscriber;

/**
 * Created by nicolas.m on 9/13/2017.
 */

public class NewLocationReceiver extends BroadcastReceiver {

    private static final String TAG = "NewLocationReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        final Location newLocation = intent.getParcelableExtra("NEW_LOCATION");

        if (newLocation != null) {

            App.businessManager.getDatabaseManager().getLastLocation().subscribe(new Subscriber<RealmLocation>() {
                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {

                    Log.e(TAG, e.getMessage());
                    e.printStackTrace();

                }

                @Override
                public void onNext(RealmLocation savedLocation) {

                    // Nunca deberia ser nula, porque al menos tiene que estar la posicion de comienzo...
                    if (savedLocation != null) {

                        String extraInfo = "";

                        ParametersModel parametros = ParametrosUtils.getInstance().getParametros();

                        boolean saveLocation = false;

                        if (parametros.isControlRumbo()) {

                            float difRumbo = Math.abs(savedLocation.getBearing() - newLocation.getBearing());
                            if (difRumbo > 180)
                                difRumbo = 360 - difRumbo;

                            if (difRumbo > parametros.getRumboGrados()) {

                                double dist = LocationUtils.distance(newLocation.getLatitude(),
                                        savedLocation.getLatitude(),
                                        newLocation.getLongitude(),
                                        savedLocation.getLongitude(),
                                        newLocation.getAltitude(),
                                        savedLocation.getAltitude());

                                saveLocation = dist > parametros.getRumboDistancia();

                                if (saveLocation)
                                    extraInfo = "Rumbo, newRumbo = " + newLocation.getBearing() + ", oldRumbo = " + savedLocation.getBearing() + ", difRumbo = " + difRumbo + ", distancia = " + dist + ", compareTO = " + BaseFunctions.formatDate(savedLocation.getTime());
                            }

                        }

                        float velActual = newLocation.getSpeed();

                        if (!saveLocation && parametros.isControlTiempoVelocidad()) {

                            long frecuencia = parametros.getTiempo2();

                            if (velActual > parametros.getVelocidad2()) {

                                frecuencia = parametros.getTiempo3();

                            } else if (velActual <= parametros.getVelocidad1()) {

                                frecuencia = parametros.getTiempo1();
                            }

                            long diffSegundos = Math.abs(newLocation.getTime() - savedLocation.getTime()) / 1000;

                            saveLocation = diffSegundos > frecuencia;

                            if (saveLocation)
                                extraInfo = "TiempoVelocidad, velActual = " + velActual + ", diffSegundos = " + diffSegundos + ", frecuencia = " + frecuencia + ", compareTO = " + BaseFunctions.formatDate(savedLocation.getTime());

                        }

                        if (!saveLocation && parametros.isControlDistanciaVelocidad()) {

                            float metros = parametros.getDistancia2();

                            if (velActual > parametros.getVelocidad4()) {

                                metros = parametros.getDistancia3();

                            } else if (velActual <= parametros.getVelocidad3()) {

                                metros = parametros.getDistancia1();
                            }

                            double dist = LocationUtils.distance(newLocation.getLatitude(),
                                    savedLocation.getLatitude(),
                                    newLocation.getLongitude(),
                                    savedLocation.getLongitude(),
                                    newLocation.getAltitude(),
                                    savedLocation.getAltitude());

                            saveLocation = dist > metros;

                            if (saveLocation)
                                extraInfo = "DistanciaVelocidad, velActual = " + velActual + ", distancia = " + dist + ", metros = " + metros + ", compareTO = " + BaseFunctions.formatDate(savedLocation.getTime());

                        }

                        if (saveLocation) {

                            App.businessManager.getDatabaseManager().saveLocation(new RealmLocation(newLocation, savedLocation.getTripId(), ParametrosUtils.getInstance().getVersion(), extraInfo)).subscribe(new Subscriber<RealmLocation>() {
                                @Override
                                public void onCompleted() {

                                }

                                @Override
                                public void onError(Throwable e) {

                                    Log.e(TAG, e.getMessage());
                                    e.printStackTrace();

                                }

                                @Override
                                public void onNext(RealmLocation realmLocation) {

                                    Log.d(TAG, "saveLocation OK");
                                }
                            });

                        }

                    }

                }

            });

        }

    }

}
