package com.fepsa.remises.business.data.database.resolver;


import com.fepsa.remises.business.data.database.model.RealmLocation;
import com.fepsa.remises.business.data.database.model.RealmTrip;

import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by tksko on 24/09/2017.
 */

public class GetTripLocationsDBResolver extends BaseDBResolver {

    public Observable<List<RealmLocation>> executeQuery(final Long tripId) {

        return Observable.create(
                new Observable.OnSubscribe<List<RealmLocation>>() {

                    @Override
                    public void call(final Subscriber<? super List<RealmLocation>> subscriber) {

                        RealmTrip theTrip = realm.where(RealmTrip.class).equalTo("startDate", tripId).findFirst();

                        subscriber.onNext(theTrip.getLocations().where().sort("time").findAll());

                        subscriber.onCompleted();
                    }
                }
        )
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
