package com.fepsa.remises.business.data.database.model;

import io.realm.RealmObject;

/**
 * Created by nicolas.m on 11/3/2017.
 */

public class RealmPassenger extends RealmObject {

    private RealmPerson person;

    private RealmLocation posSube;

    private RealmLocation posBaja;

    public RealmPassenger() {

        this.person = null;
        this.posSube = null;
        this.posBaja = null;
    }

    public RealmPerson getPerson() {
        return person;
    }

    public void setPerson(RealmPerson person) {
        this.person = person;
    }

    public RealmLocation getPosSube() {
        return posSube;
    }

    public void setPosSube(RealmLocation posSube) {
        this.posSube = posSube;
    }

    public RealmLocation getPosBaja() {
        return posBaja;
    }

    public void setPosBaja(RealmLocation posBaja) {
        this.posBaja = posBaja;
    }

}
