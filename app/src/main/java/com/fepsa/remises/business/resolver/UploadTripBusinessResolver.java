package com.fepsa.remises.business.resolver;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fepsa.remises.business.data.backend.BackendManager;
import com.fepsa.remises.business.data.backend.request.AgregarViajeRequest;
import com.fepsa.remises.business.data.backend.response.BaseResponse;
import com.fepsa.remises.business.data.database.DatabaseManager;
import com.fepsa.remises.business.data.database.model.RealmTrip;
import com.fepsa.remises.config.AppConstants;
import com.fepsa.remises.ui.application.App;
import com.fepsa.remises.ui.util.BaseFunctions;

import java.util.List;

import rx.Subscriber;

/**
 * Created by tksko on 12/18/2017.
 */

public class UploadTripBusinessResolver extends BaseBusinessResolver<Boolean, BaseResponse> {

    private RealmTrip theTrip;

    public UploadTripBusinessResolver(DatabaseManager databaseManager, BackendManager backendManager) {
        super(databaseManager, backendManager);
    }

    @Override
    protected void callDatabase() {

        databaseManager.getPendingTrips().subscribe(new Subscriber<List<RealmTrip>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

                subscriber.onError(e);

            }

            @Override
            public void onNext(List<RealmTrip> realmTrips) {

                if (realmTrips.size() > 0) {

                    theTrip = realmTrips.get(0);

                    callBackend();

                } else {

                    subscriber.onNext(true);

                    subscriber.onCompleted();

                }

            }
        });

    }

    @Override
    protected void callBackend() {

        AgregarViajeRequest request = new AgregarViajeRequest(theTrip);

        try {

            String jsonRequest = BaseFunctions.getMapper().writeValueAsString(request);

            backendManager.uploadTrip(
                    jsonRequest
            ).subscribe(this);

        } catch (JsonProcessingException e) {

            subscriber.onError(e);

        }

    }

    @Override
    protected void onSuccessCallActions(BaseResponse response) {

        databaseManager.removeTrip(theTrip.getStartDate()).subscribe(new Subscriber<Boolean>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

                subscriber.onError(e);

            }

            @Override
            public void onNext(Boolean aBoolean) {

                getPendingTrips();

            }

        });

    }

    private void getPendingTrips() {

        databaseManager.getPendingTrips().subscribe(new Subscriber<List<RealmTrip>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable throwable) {

                subscriber.onError(throwable);

            }

            @Override
            public void onNext(List<RealmTrip> realmTrips) {

                Intent pendingTrips = new Intent(AppConstants.BroadcastActions.PENDING_TRIPS);

                pendingTrips.putExtra("TRIPS_COUNT", realmTrips.size());

                LocalBroadcastManager.getInstance(App.applicationContext).sendBroadcast(pendingTrips);

                subscriber.onNext(true);

                subscriber.onCompleted();

            }

        });


    }

}
