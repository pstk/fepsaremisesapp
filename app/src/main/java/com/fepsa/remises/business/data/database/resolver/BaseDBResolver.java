package com.fepsa.remises.business.data.database.resolver;

import com.fepsa.remises.business.data.database.RealmDatabase;

import io.realm.Realm;

/**
 * Created by tksko on 02/07/2017.
 */

public class BaseDBResolver {

    protected Realm realm;

    public BaseDBResolver() {

        this.realm = RealmDatabase.getInstance().getRealm();
    }

}