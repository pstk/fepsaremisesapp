package com.fepsa.remises.business.data.database.resolver;


import com.fepsa.remises.business.data.database.model.RealmTrip;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by tksko on 24/09/2017.
 */

public class GetActiveTripDBResolver extends BaseDBResolver {

    public Observable<RealmTrip> executeQuery() {

        return Observable.create(
                new Observable.OnSubscribe<RealmTrip>() {

                    @Override
                    public void call(final Subscriber<? super RealmTrip> subscriber) {

                        RealmTrip trip = realm
                                .where(RealmTrip.class)
                                .isNull("endDate").findFirst();

                        subscriber.onNext(trip);

                        subscriber.onCompleted();
                    }

                }

        )
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
