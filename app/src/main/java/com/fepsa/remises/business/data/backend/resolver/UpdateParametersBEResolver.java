package com.fepsa.remises.business.data.backend.resolver;

import com.fepsa.remises.business.data.backend.model.ParametersEntity;

import java.util.Map;

/**
 * Created by tksko on 02/07/2017.
 */

public class UpdateParametersBEResolver extends BaseBEResolver<ParametersEntity> {

    @Override
    protected void getDataFromBackend(Map<String, Object> params) {

        client.updateParametros().enqueue(this);

    }

}
