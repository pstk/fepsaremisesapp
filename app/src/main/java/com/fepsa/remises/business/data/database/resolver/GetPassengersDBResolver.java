package com.fepsa.remises.business.data.database.resolver;


import com.fepsa.remises.business.data.database.model.RealmPassenger;
import com.fepsa.remises.business.data.database.model.RealmTrip;

import java.util.List;

import io.realm.Sort;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by tksko on 24/09/2017.
 */

public class GetPassengersDBResolver extends BaseDBResolver {

    public Observable<List<RealmPassenger>> executeQuery(final Long tripId) {

        return Observable.create(
                new Observable.OnSubscribe<List<RealmPassenger>>() {

                    @Override
                    public void call(final Subscriber<? super List<RealmPassenger>> subscriber) {


                        RealmTrip theTrip = realm.where(RealmTrip.class).equalTo("startDate", tripId).findFirst();

                        if (theTrip != null) {

                            subscriber.onNext(theTrip.getPassengers().where().isNull("posBaja").sort("person.name", Sort.ASCENDING).findAll());

                            subscriber.onCompleted();

                        } else {

                            subscriber.onError(new Exception("Viaje inválido"));
                        }

                    }

                }
        )
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
