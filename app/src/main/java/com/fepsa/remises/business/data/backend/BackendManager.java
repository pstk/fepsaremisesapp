package com.fepsa.remises.business.data.backend;

import com.fepsa.remises.business.data.backend.model.LoginEntity;
import com.fepsa.remises.business.data.backend.model.ParametersEntity;
import com.fepsa.remises.business.data.backend.resolver.EmptyBEResolver;
import com.fepsa.remises.business.data.backend.resolver.LoginChoferBEResolver;
import com.fepsa.remises.business.data.backend.resolver.SnapToRoadBEResolver;
import com.fepsa.remises.business.data.backend.resolver.UpdateParametersBEResolver;
import com.fepsa.remises.business.data.backend.resolver.UpdatePersonsBEResolver;
import com.fepsa.remises.business.data.backend.resolver.UploadTripBEResolver;
import com.fepsa.remises.business.data.backend.response.BaseResponse;
import com.fepsa.remises.business.data.backend.response.SnapToRoadResponse;
import com.fepsa.remises.business.data.backend.response.UpdatePersonasResponse;

import java.util.HashMap;
import java.util.Map;

import rx.Observable;

/**
 * Created by tksko on 02/07/2017.
 */

public class BackendManager {

    public BackendManager() {

    }

    public Observable<BaseResponse> empty() {

        EmptyBEResolver backendResolver = new EmptyBEResolver();

        return backendResolver.makeCall(null);

    }

    public Observable<SnapToRoadResponse> snapToRoad(String path) {

        SnapToRoadBEResolver backendResolver = new SnapToRoadBEResolver();

        Map<String, Object> params = new HashMap<>();
        params.put("PATH", path);

        return backendResolver.makeCall(params);

    }

    // Login Choferes

    public Observable<LoginEntity> loginChofer(String nombre, String clave, String dni) {

        LoginChoferBEResolver backendResolver = new LoginChoferBEResolver();

        Map<String, Object> params = new HashMap<>();
        params.put("nombre", nombre);
        params.put("clave", clave);
        params.put("dni", dni);

        return backendResolver.makeCall(params);

    }

    // Persons

    public Observable<UpdatePersonasResponse> updatePersons() {

        UpdatePersonsBEResolver backendResolver = new UpdatePersonsBEResolver();

        return backendResolver.makeCall(null);

    }

    // Parameters

    public Observable<ParametersEntity> updateParameters() {

        UpdateParametersBEResolver backendResolver = new UpdateParametersBEResolver();

        return backendResolver.makeCall(null);

    }

    // Upload Trip

    public Observable<BaseResponse> uploadTrip(String jsonRequest) {

        UploadTripBEResolver backendResolver = new UploadTripBEResolver();

        Map<String, Object> params = new HashMap<>();
        params.put("jsonRequest", jsonRequest);

        return backendResolver.makeCall(params);

    }

}
