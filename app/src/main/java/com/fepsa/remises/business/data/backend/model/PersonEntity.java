package com.fepsa.remises.business.data.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by tksko on 02/07/2017.
 */

@JsonIgnoreProperties(
        ignoreUnknown = true
)
public class PersonEntity {

    @JsonProperty("pasajeroId")
    private long id;

    @JsonProperty("nombre")
    private String name;

    @JsonProperty("legajo")
    private String legajo;

    public PersonEntity() {

        this.id = 0;
        this.name = "";
        this.legajo = "";

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLegajo() {
        return legajo;
    }

    public void setLegajo(String legajo) {
        this.legajo = legajo;
    }
}