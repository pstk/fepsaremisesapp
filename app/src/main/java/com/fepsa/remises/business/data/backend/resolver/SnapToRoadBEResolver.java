package com.fepsa.remises.business.data.backend.resolver;

import com.fepsa.remises.business.data.backend.response.SnapToRoadResponse;
import com.fepsa.remises.config.AppConstants;

import java.util.Map;

/**
 * Created by tksko on 02/07/2017.
 */

public class SnapToRoadBEResolver extends BaseBEResolver<SnapToRoadResponse> {

    @Override
    protected void getDataFromBackend(Map<String, Object> params) {

        client.snapToRoad(
                AppConstants.Endpoint.GOOGLE_SNAP_TO_ROAD +
                        "?path=" +
                        String.valueOf(params.get("PATH")) +
                        "&key=" + AppConstants.GOOGLE_SNAP_TO_ROAD_KEY
        ).enqueue(this);

    }

}
