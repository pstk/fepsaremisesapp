package com.fepsa.remises.business.data.database;

/**
 * Created by tksko on 02/07/2017.
 */

public interface Cachable {

    void setExpiration();

    boolean isExpired();

}