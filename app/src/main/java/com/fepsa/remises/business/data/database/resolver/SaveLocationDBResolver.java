package com.fepsa.remises.business.data.database.resolver;


import com.fepsa.remises.business.data.database.model.RealmLocation;
import com.fepsa.remises.business.data.database.model.RealmTrip;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by tksko on 24/09/2017.
 */

public class SaveLocationDBResolver extends BaseDBResolver {

    public Observable<RealmLocation> executeQuery(final RealmLocation location) {

        return Observable.create(
                new Observable.OnSubscribe<RealmLocation>() {

                    @Override
                    public void call(final Subscriber<? super RealmLocation> subscriber) {

                        // Active trip
                        RealmTrip theTrip = realm
                                .where(RealmTrip.class)
                                .isNull("endDate").findFirst();

                        if (theTrip != null) {

                            realm.beginTransaction();

                            RealmLocation savedLocation = realm.copyToRealm(location);

                            //SharedPrefs.saveLong(SharedPrefs.Constants.LATEST_SAVED_DATE, savedLocation.getmTime());

                            theTrip.getLocations().add(savedLocation);

                            realm.commitTransaction();

                            subscriber.onNext(savedLocation);

                            subscriber.onCompleted();

                        } else {

                            subscriber.onError(new Exception("No hay un viaje en curso."));

                        }

                    }
                }
        )
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
