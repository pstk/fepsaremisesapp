package com.fepsa.remises.business.data.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by tksko on 4/10/2017.
 */

@JsonIgnoreProperties(
        ignoreUnknown = true
)
public class SnappedPointEntity {

    @JsonProperty("location")
    private LocationEntity location;

    public SnappedPointEntity() {

        this.location = new LocationEntity();
    }

    public LocationEntity getLocation() {
        return location;
    }

    public void setLocation(LocationEntity location) {
        this.location = location;
    }

}
