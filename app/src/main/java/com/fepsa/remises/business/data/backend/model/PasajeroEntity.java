package com.fepsa.remises.business.data.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fepsa.remises.business.data.database.model.RealmPassenger;

/**
 * Created by nicolas.m on 12/18/2017.
 */

@JsonIgnoreProperties(
        ignoreUnknown = true
)
public class PasajeroEntity {

    private Long pasajeroId;

    private String pasajeroNombre;

    private Long posSube;

    private Long posBaja;

    public PasajeroEntity(RealmPassenger thePassenger) {

        if (thePassenger.getPerson().isLocal()) {
            this.pasajeroId = null;
            this.pasajeroNombre = thePassenger.getPerson().getName();
        } else {
            this.pasajeroId = thePassenger.getPerson().getId();
            this.pasajeroNombre = "";
        }

        this.posSube = thePassenger.getPosSube().getTime();

        this.posBaja = thePassenger.getPosBaja().getTime();

    }

    public PasajeroEntity(Long pasajeroId, Long posSube, Long posBaja) {
        this.pasajeroId = pasajeroId;
        this.pasajeroNombre = null;
        this.posSube = posSube;
        this.posBaja = posBaja;
    }

    public PasajeroEntity(String pasajeroNombre, Long posSube, Long posBaja) {
        this.pasajeroId = null;
        this.pasajeroNombre = pasajeroNombre;
        this.posSube = posSube;
        this.posBaja = posBaja;
    }

    public Long getPasajeroId() {
        return pasajeroId;
    }

    public void setPasajeroId(Long pasajeroId) {
        this.pasajeroId = pasajeroId;
    }

    public String getPasajeroNombre() {
        return pasajeroNombre;
    }

    public void setPasajeroNombre(String pasajeroNombre) {
        this.pasajeroNombre = pasajeroNombre;
    }

    public Long getPosSube() {
        return posSube;
    }

    public void setPosSube(Long posSube) {
        this.posSube = posSube;
    }

    public Long getPosBaja() {
        return posBaja;
    }

    public void setPosBaja(Long posBaja) {
        this.posBaja = posBaja;
    }

}
