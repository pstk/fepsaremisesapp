package com.fepsa.remises.business.resolver;

import com.fepsa.remises.business.data.backend.BackendManager;
import com.fepsa.remises.business.data.backend.response.UpdatePersonasResponse;
import com.fepsa.remises.business.data.database.DatabaseManager;
import com.fepsa.remises.business.data.database.model.RealmPerson;
import com.fepsa.remises.ui.model.PersonDTO;
import com.fepsa.remises.util.ParametrosUtils;
import com.fepsa.remises.util.SharedPrefs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import rx.Subscriber;

/**
 * Created by nicolas.m on 12/5/2017.
 */

public class SearchPersonsBusinessResolver extends BaseBusinessResolver<List<PersonDTO>, UpdatePersonasResponse> {

    private List<RealmPerson> list;

    public SearchPersonsBusinessResolver(DatabaseManager databaseManager, BackendManager backendManager) {
        super(databaseManager, backendManager);
    }

    @Override
    protected void callDatabase() {

        final String query = (String) params.get("query");

        databaseManager.getPersons(query, ParametrosUtils.getInstance().getVersionPersonas()).subscribe(new Subscriber<List<RealmPerson>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

                subscriber.onError(e);

            }

            @Override
            public void onNext(List<RealmPerson> realmPersons) {

                list = realmPersons;

                // Perhaps the sync fail, we try to get users now! (only if listing the full list)
                if (forceBackend || (list.size() == 0 && (query == null || query.trim().length() == 0))) {

                    callBackend();

                } else {

                    emitModel();

                }

            }

        });

    }

    @Override
    protected void callBackend() {

        backendManager.updatePersons().subscribe(new Subscriber<UpdatePersonasResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

                emitModel();

            }

            @Override
            public void onNext(final UpdatePersonasResponse response) {

                databaseManager.savePersons(RealmPerson.fromEntityList(response.getPersonas(), response.getVersion())).subscribe(new Subscriber<List<RealmPerson>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        emitModel();

                    }

                    @Override
                    public void onNext(List<RealmPerson> realmPersons) {

                        list = realmPersons;

                        SharedPrefs.saveInt(SharedPrefs.Constants.PERSONS_DB_VERSION, response.getVersion());

                        emitModel();

                    }

                });

            }

        });

    }

    private void emitModel() {

        List<PersonDTO> persons = new ArrayList<>();

        for (RealmPerson person : list) {

            persons.add(new PersonDTO(person));

        }

        // Sort personas again.... realm is buged :S
        Collections.sort(persons);

        subscriber.onNext(persons);

        subscriber.onCompleted();

    }

}
