package com.fepsa.remises.business.data.backend.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by tksko on 02/07/2017.
 */

@JsonIgnoreProperties(
        ignoreUnknown = true
)
public class BaseResponse {

    public BaseResponse() {

    }

}
