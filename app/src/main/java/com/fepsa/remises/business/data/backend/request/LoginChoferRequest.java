package com.fepsa.remises.business.data.backend.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by tksko on 16/7/2017.
 */

@JsonIgnoreProperties(
        ignoreUnknown = true
)
public class LoginChoferRequest {

    @JsonProperty("nombre")
    private String nombre;

    @JsonProperty("clave")
    private String clave;

    @JsonProperty("dni")
    private String dni;

    public LoginChoferRequest(String nombre, String clave, String dni) {
        this.nombre = nombre;
        this.clave = clave;
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

}
