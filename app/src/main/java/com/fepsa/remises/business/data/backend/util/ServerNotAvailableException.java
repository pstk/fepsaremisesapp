package com.fepsa.remises.business.data.backend.util;

/**
 * Created by tksko on 02/07/2017.
 */

public class ServerNotAvailableException extends Exception {

    public ServerNotAvailableException() {

        super("Server not Available. Please Retry later");

    }

}
