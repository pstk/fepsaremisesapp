package com.fepsa.remises.business.data.backend.resolver;

import com.fepsa.remises.business.data.backend.response.UpdatePersonasResponse;

import java.util.Map;

/**
 * Created by tksko on 02/07/2017.
 */

public class UpdatePersonsBEResolver extends BaseBEResolver<UpdatePersonasResponse> {

    @Override
    protected void getDataFromBackend(Map<String, Object> params) {

        client.updatePersonas().enqueue(this);

    }

}
