package com.fepsa.remises.business.data.database.resolver;


import com.fepsa.remises.business.data.database.model.RealmLocation;
import com.fepsa.remises.business.data.database.model.RealmPassenger;
import com.fepsa.remises.business.data.database.model.RealmPerson;
import com.fepsa.remises.business.data.database.model.RealmTrip;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by tksko on 24/09/2017.
 */

public class SavePassengerDBResolver extends BaseDBResolver {

    public Observable<RealmPassenger> executeQuery(final long tripId, final RealmPassenger passenger) {

        return Observable.create(
                new Observable.OnSubscribe<RealmPassenger>() {

                    @Override
                    public void call(final Subscriber<? super RealmPassenger> subscriber) {

                        realm.beginTransaction();

                        RealmTrip theTrip = realm.where(RealmTrip.class).equalTo("startDate", tripId).findFirst();

                        // Necesitamos saber si la persona existe en la DB
                        RealmPerson thePerson = realm.where(RealmPerson.class).equalTo("legajo", passenger.getPerson().getLegajo()).findFirst();

                        // Instanciamos la persona ya persistida.....
                        if (thePerson != null) {
                            passenger.setPerson(thePerson);
                        }

                        RealmPassenger thePassenger = theTrip.getPassengers().where().equalTo("person.legajo", passenger.getPerson().getLegajo()).isNull("posBaja").findFirst();

                        if (thePassenger == null) {

                            RealmLocation theLocation = theTrip.getLocations().where().equalTo("time", passenger.getPosSube().getTime()).equalTo("tripId", tripId).findFirst();

                            if (theLocation == null) {

                                theLocation = realm.copyToRealm(passenger.getPosSube());

                                theTrip.getLocations().add(theLocation);
                            }

                            passenger.setPosSube(theLocation);

                            theTrip.getPassengers().add(passenger);

                            realm.commitTransaction();

                            subscriber.onNext(passenger);

                            subscriber.onCompleted();

                        } else {

                            // Pasajero repetido!

                            realm.cancelTransaction();

                            subscriber.onError(new Exception("El pasajero " + passenger.getPerson().getName() + " ya se encuentra viajando."));

                            subscriber.onCompleted();

                        }

                    }
                }
        )
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
