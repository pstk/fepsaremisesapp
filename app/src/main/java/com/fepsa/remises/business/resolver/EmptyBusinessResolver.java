package com.fepsa.remises.business.resolver;

import com.fepsa.remises.business.data.backend.BackendManager;
import com.fepsa.remises.business.data.backend.response.BaseResponse;
import com.fepsa.remises.business.data.database.DatabaseManager;
import com.fepsa.remises.business.model.EmptyModel;

/**
 * Created by tksko on 02/07/2017.
 */

public class EmptyBusinessResolver extends BaseBusinessResolver<EmptyModel, BaseResponse> {

    public EmptyBusinessResolver(DatabaseManager databaseManager, BackendManager backendManager) {
        super(databaseManager, backendManager);
    }

    @Override
    protected void callBackend() {

        backendManager.empty().subscribe(this);

    }

}
