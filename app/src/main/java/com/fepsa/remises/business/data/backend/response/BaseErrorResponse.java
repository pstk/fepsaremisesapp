package com.fepsa.remises.business.data.backend.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by tksko on 02/07/2017.
 */

@JsonIgnoreProperties(
        ignoreUnknown = true
)
public class BaseErrorResponse {

    @JsonProperty("error")
    private String error;

    public BaseErrorResponse() {
        this.error = "";
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

}
