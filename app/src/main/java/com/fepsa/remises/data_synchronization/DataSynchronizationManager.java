package com.fepsa.remises.data_synchronization;

import android.content.Context;
import android.content.Intent;

import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by tksko on 02/07/2017.
 */

public class DataSynchronizationManager {

    private static Timer timer = new Timer();

    /**
     * 60 seconds
     */
    public static final int DATA_SYNCHRONIZATION_INTERVAL = 60000;

    public static final int RETRY_LIMIT = 999999;

    public static void initialise(Context context) {

        resetDataSyncService();

        scheduleSynchronizationTask(context);

    }

    private static void resetDataSyncService() {

        DataSyncService.reset();

    }

    private static void scheduleSynchronizationTask(final Context context) {

        if (timer != null) {
            timer.cancel();
            timer = new Timer();
        }

        TimerTask timerTask = new TimerTask() {

            @Override
            public void run() {

                FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(context));

                Job myJob = dispatcher.newJobBuilder()
                        .setService(DataSyncService.class) // the JobService that will be called
                        .setTag(DataSyncService.class.getSimpleName())        // uniquely identifies the job
                        .build();

                dispatcher.mustSchedule(myJob);
            }

        };

        timer.schedule(timerTask, 0, DATA_SYNCHRONIZATION_INTERVAL);

    }

}