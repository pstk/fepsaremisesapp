package com.fepsa.remises.data_synchronization;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;

import com.fepsa.remises.config.AppConstants;
import com.fepsa.remises.data_synchronization.service.ParametersSyncService;
import com.fepsa.remises.data_synchronization.service.PendingTripsSyncService;
import com.fepsa.remises.data_synchronization.service.PersonsSyncService;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;

/**
 * Created by tksko on 02/07/2017.
 */

public class DataSyncService extends JobService {

    private static FirebaseJobDispatcher dispatcher;

    @Override
    public void onCreate() {

        super.onCreate();

        dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
    }

    @Override
    public boolean onStartJob(@NonNull JobParameters job) {

        Log.i(AppConstants.APP_TAG, "-------------------DATA SYNC SERVICE STARTED-------------------");

        startParametersService();

        startPersonsService();

        startPendingTripsService();

        return false;
    }

    @Override
    public boolean onStopJob(@NonNull JobParameters job) {
        return false;
    }

    public static void reset() {

        resetParametersService();

        resetPersonsService();

        resetPendingTripsService();

    }

    private static void resetPersonsService() {

        PersonsSyncService.reset();

    }

    private void startPersonsService() {

        if (!PersonsSyncService.isRunning()) {

            Job myJob = dispatcher.newJobBuilder()
                    .setService(PersonsSyncService.class) // the JobService that will be called
                    .setTag(PersonsSyncService.class.getSimpleName())        // uniquely identifies the job
                    .build();

            dispatcher.mustSchedule(myJob);

        }

    }

    private static void resetParametersService() {

        ParametersSyncService.reset();

    }

    private void startParametersService() {

        if (!ParametersSyncService.isRunning()) {

            Job myJob = dispatcher.newJobBuilder()
                    .setService(ParametersSyncService.class) // the JobService that will be called
                    .setTag(PersonsSyncService.class.getSimpleName())        // uniquely identifies the job
                    .build();

            dispatcher.mustSchedule(myJob);

        }

    }

    private static void resetPendingTripsService() {

        PendingTripsSyncService.reset();

    }

    private void startPendingTripsService() {

        if (!PendingTripsSyncService.isRunning()) {

            Job myJob = dispatcher.newJobBuilder()
                    .setService(PendingTripsSyncService.class) // the JobService that will be called
                    .setTag(PendingTripsSyncService.class.getSimpleName())        // uniquely identifies the job
                    .build();

            dispatcher.mustSchedule(myJob);

        }

    }

}