package com.fepsa.remises.data_synchronization.service;

import android.content.Intent;
import android.util.Log;

import com.fepsa.remises.business.data.backend.util.CheckConnection;
import com.fepsa.remises.config.AppConstants;
import com.fepsa.remises.data_synchronization.DataSynchronizationManager;
import com.fepsa.remises.util.AuthUtils;
import com.fepsa.remises.util.SharedPrefs;
import com.firebase.jobdispatcher.JobParameters;

import rx.Subscriber;

/**
 * Created by tksko on 02/07/2017.
 */

public abstract class BaseSyncService<T extends Object> extends NonStopIntentService {

    protected String nextCallTimeKey;

    protected String retryCountKey;

    protected String lastSuccessfulCallTimeKey;

    protected String lastFailedCallTimeKey;

    protected long intervalBetweenCalls;

    protected long nextCallTime;

    protected int retryCount;

    protected String logTag;

    public BaseSyncService(String name,
                           String nextCallTimeKey,
                           String retryCountKey,
                           String lastSuccessfulCallTimeKey,
                           String lastFailedCallTimeKey,
                           long intervalBetweenCalls,
                           String logTag) {

        super(name);

        this.nextCallTimeKey = nextCallTimeKey;
        this.retryCountKey = retryCountKey;
        this.lastSuccessfulCallTimeKey = lastSuccessfulCallTimeKey;
        this.lastFailedCallTimeKey = lastFailedCallTimeKey;
        this.intervalBetweenCalls = intervalBetweenCalls;
        this.logTag = logTag;
    }

    public BaseSyncService(String name,
                           String logTag,
                           String lastSuccessfulCallTimeKey) {

        super(name);
        this.logTag = logTag;
        this.lastSuccessfulCallTimeKey = lastSuccessfulCallTimeKey;

    }

    @Override
    protected void onHandleJobParameters(JobParameters params) {

        nextCallTime = SharedPrefs.loadLong(nextCallTimeKey, System.currentTimeMillis());

        long currentTime = System.currentTimeMillis();

        retryCount = SharedPrefs.loadInt(retryCountKey, 0);

        boolean isNetworkConnectivityAvailable = CheckConnection.getInstance(this).isOnline();

        boolean isLoggedIn = AuthUtils.isLoggedIn();

        boolean shouldSync =
                isNetworkConnectivityAvailable
                        && isLoggedIn
                        && (currentTime >= nextCallTime)
                        && (retryCount < DataSynchronizationManager.RETRY_LIMIT);

        if (shouldSync) {

            startSync();
        } else {

            if (retryCount == DataSynchronizationManager.RETRY_LIMIT) {

                // after a RETRY_LIMIT number of failed attempts, reset the number of failed attempts to perform a sync
                // and wait the time equal to INTERVAL_BETWEEN_CALLS before making a new attempt

                setupNextCall();
            }

            stopRunning();
        }
    }

    private void setupNextCall() {

        // reset the number of failed attempts to perform a sync
        SharedPrefs.saveInt(retryCountKey, 0);

        // save the ideal time of the next sync attempt
        SharedPrefs.saveLong(nextCallTimeKey, nextCallTime
                + intervalBetweenCalls);
    }

    private void addNewFailedAttempt() {

        SharedPrefs.saveInt(retryCountKey, retryCount + 1);
    }

    protected void startSync() {

        Log.i(AppConstants.APP_TAG, "-------------------STARTING " + logTag + " SYNCHRONIZATION-------------------");

        syncData(createSubscriber());
    }

    protected abstract void syncData(Subscriber<T> subscriber);

    /**
     * Notifies subclasses that the current sync has failed,
     * in case they need to do some extra work.
     */
    protected abstract void onSyncFail(Throwable e);

    /**
     * Notifies subclasses that the current sync was successful,
     * in case they need to do some extra work.
     */
    protected abstract void onSyncSuccess(T result);

    private Subscriber<T> createSubscriber() {

        return new Subscriber<T>() {

            @Override
            public void onCompleted() {

                unsubscribe();
            }

            @Override
            public void onError(Throwable e) {

                try {

                    onSyncFail(e);

                    finish(false);

                    unsubscribe();

                } catch (Exception e1) {
                    e1.printStackTrace();
                    stopRunning();
                }
            }

            @Override
            public void onNext(T result) {

                onSyncSuccess(result);

                finish(true);
            }
        };
    }

    protected void finish(boolean wasSyncSuccessful) {

        Log.i(AppConstants.APP_TAG, "-------------------ENDING " + logTag + " SYNCHRONIZATION (SUCCESS: " +
                wasSyncSuccessful +
                ") -------------------");

        long currentTime = System.currentTimeMillis();

        if (wasSyncSuccessful) {

            setupNextCall();

            // save the current time as the time of the last successful sync (only for analytics purposes)
            SharedPrefs.saveLong(lastSuccessfulCallTimeKey, currentTime);

        } else {

            addNewFailedAttempt();

            // save the current time as the time of the last failed sync (only for analytics purposes)
            SharedPrefs.saveLong(lastFailedCallTimeKey, currentTime);

        }

        stopRunning();
    }
}
