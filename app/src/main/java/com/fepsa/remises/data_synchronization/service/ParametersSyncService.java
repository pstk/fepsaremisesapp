package com.fepsa.remises.data_synchronization.service;

import com.fepsa.remises.business.data.backend.model.ParametersEntity;
import com.fepsa.remises.business.model.ParametersModel;
import com.fepsa.remises.ui.application.App;
import com.fepsa.remises.util.ParametrosUtils;
import com.fepsa.remises.util.SharedPrefs;

import rx.Subscriber;

/**
 * Created by tksko on 02/07/2017.
 */

public class ParametersSyncService extends BaseSyncService<ParametersEntity> {

    private static final String PARAMETERS_SYNC_SERVICE_NEXT_CALL_TIME =
            "PARAMETERS_SYNC_SERVICE_NEXT_CALL_TIME";

    private static final String PARAMETERS_SYNC_SERVICE_RETRY_COUNT =
            "PARAMETERS_SYNC_SERVICE_RETRY_COUNT";

    private static final String PARAMETERS_SYNC_SERVICE_LAST_SUCCESSFUL_CALL_TIME =
            "PARAMETERS_SYNC_SERVICE_LAST_SUCCESSFUL_CALL_TIME";

    private static final String PARAMETERS_SYNC_SERVICE_LAST_FAILED_CALL_TIME =
            "PARAMETERS_SYNC_SERVICE_LAST_FAILED_CALL_TIME";

    private static final String LOG_TAG = "PARAMETERS_SYNC_SERVICE";

    /**
     * 6 Hours
     */
    private static final long PARAMETERS_SYNCHRONIZATION_INTERVAL_BETWEEN_CALLS = 3600000 * 6;

    public ParametersSyncService() {

        super(
                ParametersSyncService.class.getName(),
                PARAMETERS_SYNC_SERVICE_NEXT_CALL_TIME,
                PARAMETERS_SYNC_SERVICE_RETRY_COUNT,
                PARAMETERS_SYNC_SERVICE_LAST_SUCCESSFUL_CALL_TIME,
                PARAMETERS_SYNC_SERVICE_LAST_FAILED_CALL_TIME,
                PARAMETERS_SYNCHRONIZATION_INTERVAL_BETWEEN_CALLS,
                LOG_TAG
        );

    }

    public static void reset() {

        // reset the number of failed attempts to perform a sync
        SharedPrefs.saveInt(PARAMETERS_SYNC_SERVICE_RETRY_COUNT, 0);

        // reset the ideal date of the next execution of this sync service
        SharedPrefs.saveLong(PARAMETERS_SYNC_SERVICE_NEXT_CALL_TIME, System.currentTimeMillis());

    }

    @Override
    protected void syncData(Subscriber<ParametersEntity> subscriber) {

        App.businessManager.getBackendManager().updateParameters()
                .subscribe(subscriber);

    }

    @Override
    protected void onSyncFail(Throwable e) {

    }

    @Override
    protected void onSyncSuccess(ParametersEntity parameters) {

        ParametrosUtils.getInstance().setParametros(new ParametersModel(parameters));

    }

}
