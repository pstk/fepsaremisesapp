package com.fepsa.remises.data_synchronization.service;

import android.content.Intent;

import com.fepsa.remises.business.data.backend.util.CheckConnection;
import com.fepsa.remises.ui.application.App;
import com.fepsa.remises.util.AuthUtils;
import com.fepsa.remises.util.ParametrosUtils;
import com.fepsa.remises.util.SharedPrefs;
import com.firebase.jobdispatcher.JobParameters;

import rx.Subscriber;

/**
 * Created by tksko on 02/07/2017.
 */

public class PersonsSyncService extends BaseSyncService<Boolean> {

    private static final String PERSONS_SYNC_SERVICE_NEXT_CALL_TIME =
            "PERSONS_SYNC_SERVICE_NEXT_CALL_TIME";

    private static final String PERSONS_SYNC_SERVICE_RETRY_COUNT =
            "PERSONS_SYNC_SERVICE_RETRY_COUNT";

    private static final String PERSONS_SYNC_SERVICE_LAST_SUCCESSFUL_CALL_TIME =
            "PERSONS_SYNC_SERVICE_LAST_SUCCESSFUL_CALL_TIME";

    private static final String PERSONS_SYNC_SERVICE_LAST_FAILED_CALL_TIME =
            "PERSONS_SYNC_SERVICE_LAST_FAILED_CALL_TIME";

    private static final String LOG_TAG = "PERSONS_SYNC_SERVICE";

    /**
     * 6 Hours
     */
    private static final long PERSONS_SYNCHRONIZATION_INTERVAL_BETWEEN_CALLS = 3600000 * 6;

    public PersonsSyncService() {

        super(
                PersonsSyncService.class.getName(),
                PERSONS_SYNC_SERVICE_NEXT_CALL_TIME,
                PERSONS_SYNC_SERVICE_RETRY_COUNT,
                PERSONS_SYNC_SERVICE_LAST_SUCCESSFUL_CALL_TIME,
                PERSONS_SYNC_SERVICE_LAST_FAILED_CALL_TIME,
                PERSONS_SYNCHRONIZATION_INTERVAL_BETWEEN_CALLS,
                LOG_TAG
        );

    }

    @Override
    protected void onHandleJobParameters(JobParameters params) {

        boolean isNetworkConnectivityAvailable = CheckConnection.getInstance(this).isOnline();

        boolean isLoggedIn = AuthUtils.isLoggedIn();


        boolean shouldSync =
                isNetworkConnectivityAvailable
                        && isLoggedIn
                        && ParametrosUtils.getInstance().personsNeedUpgrade();

        if (shouldSync) {

            startSync();
        } else {

            stopRunning();
        }

    }

    public static void reset() {

        // reset the number of failed attempts to perform a sync
        SharedPrefs.saveInt(PERSONS_SYNC_SERVICE_RETRY_COUNT, 0);

        // reset the ideal date of the next execution of this sync service
        SharedPrefs.saveLong(PERSONS_SYNC_SERVICE_NEXT_CALL_TIME, System.currentTimeMillis());

    }

    @Override
    protected void syncData(Subscriber<Boolean> subscriber) {

        App.businessManager.updatePersons()
                .subscribe(subscriber);

    }

    @Override
    protected void onSyncFail(Throwable e) {

    }

    @Override
    protected void onSyncSuccess(Boolean success) {

    }

}
