package com.fepsa.remises.data_synchronization.service;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.fepsa.remises.business.data.database.model.RealmTrip;
import com.fepsa.remises.config.AppConstants;
import com.fepsa.remises.ui.application.App;
import com.fepsa.remises.util.AuthUtils;
import com.fepsa.remises.util.SharedPrefs;
import com.firebase.jobdispatcher.JobParameters;

import java.util.List;

import rx.Subscriber;

/**
 * Created by tksko on 02/07/2017.
 */

public class PendingTripsSyncService extends BaseSyncService<List<RealmTrip>> {

    private static final String PENDING_TRIPS_SYNC_SERVICE_NEXT_CALL_TIME =
            "PENDING_TRIPS_SYNC_SERVICE_NEXT_CALL_TIME";

    private static final String PENDING_TRIPS_SYNC_SERVICE_RETRY_COUNT =
            "PENDING_TRIPS_SYNC_SERVICE_RETRY_COUNT";

    private static final String PENDING_TRIPS_SYNC_SERVICE_LAST_SUCCESSFUL_CALL_TIME =
            "PENDING_TRIPS_SYNC_SERVICE_LAST_SUCCESSFUL_CALL_TIME";

    private static final String PENDING_TRIPS_SYNC_SERVICE_LAST_FAILED_CALL_TIME =
            "PERSONS_SYNC_SERVICE_LAST_FAILED_CALL_TIME";

    private static final String LOG_TAG = "PENDING_TRIPS_SYNC_SERVICE";

    /**
     * 5 minutes
     */
    private static final long PENDING_TRIPS_SYNCHRONIZATION_INTERVAL_BETWEEN_CALLS = 300000;

    public PendingTripsSyncService() {

        super(
                PendingTripsSyncService.class.getName(),
                PENDING_TRIPS_SYNC_SERVICE_NEXT_CALL_TIME,
                PENDING_TRIPS_SYNC_SERVICE_RETRY_COUNT,
                PENDING_TRIPS_SYNC_SERVICE_LAST_SUCCESSFUL_CALL_TIME,
                PENDING_TRIPS_SYNC_SERVICE_LAST_FAILED_CALL_TIME,
                PENDING_TRIPS_SYNCHRONIZATION_INTERVAL_BETWEEN_CALLS,
                LOG_TAG
        );

    }

    @Override
    protected void onHandleJobParameters(JobParameters params) {

        boolean isLoggedIn = AuthUtils.isLoggedIn();

        boolean shouldSync = isLoggedIn;

        if (shouldSync) {

            startSync();
        } else {

            stopRunning();
        }

    }

    public static void reset() {

        // reset the number of failed attempts to perform a sync
        SharedPrefs.saveInt(PENDING_TRIPS_SYNC_SERVICE_RETRY_COUNT, 0);

        // reset the ideal date of the next execution of this sync service
        SharedPrefs.saveLong(PENDING_TRIPS_SYNC_SERVICE_NEXT_CALL_TIME, System.currentTimeMillis());

    }

    @Override
    protected void syncData(Subscriber<List<RealmTrip>> subscriber) {

        App.businessManager.getDatabaseManager().getPendingTrips()
                .subscribe(subscriber);

    }

    @Override
    protected void onSyncFail(Throwable e) {

        e.printStackTrace();

    }

    @Override
    protected void onSyncSuccess(List<RealmTrip> trips) {

        Intent pendingTrips = new Intent(AppConstants.BroadcastActions.PENDING_TRIPS);

        pendingTrips.putExtra("TRIPS_COUNT", trips.size());

        LocalBroadcastManager.getInstance(App.applicationContext).sendBroadcast(pendingTrips);

    }

}
