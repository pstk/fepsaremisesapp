package com.fepsa.remises.data_synchronization.service;

import android.content.Intent;
import android.util.Log;

import com.fepsa.remises.business.data.backend.util.CheckConnection;
import com.fepsa.remises.config.AppConstants;
import com.fepsa.remises.util.AuthUtils;
import com.fepsa.remises.util.SharedPrefs;
import com.firebase.jobdispatcher.JobParameters;

/**
 * Created by tksko on 02/07/2017.
 */
public abstract class BaseOnDemandSyncService<T extends Object> extends BaseSyncService<T> {

    public BaseOnDemandSyncService(String name, String logTag, String lastSuccessfulCallTimeKey) {

        super(name, logTag, lastSuccessfulCallTimeKey);
    }

    @Override
    protected void onHandleJobParameters(JobParameters params) {

        if (shouldSync()) {

            startSync();
        } else {

            stopRunning();
        }

    }

    @Override
    protected void finish(boolean wasSyncSuccessful) {

        Log.i(AppConstants.APP_TAG, "-------------------ENDING " + logTag + " SYNCHRONIZATION (SUCCESS: " +
                wasSyncSuccessful +
                ") -------------------");

        if (wasSyncSuccessful) {

            // save the current time as the time of the last successful sync (only for analytics purposes)
            SharedPrefs.saveLong(lastSuccessfulCallTimeKey, System.currentTimeMillis());
        }

        stopRunning();
    }

    protected boolean shouldSync() {

        boolean isNetworkConnectivityAvailable = CheckConnection.getInstance(this).isOnline();

        boolean isLoggedIn = AuthUtils.isLoggedIn();

        return isNetworkConnectivityAvailable && isLoggedIn;

    }

}
