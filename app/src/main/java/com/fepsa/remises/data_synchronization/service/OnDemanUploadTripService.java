package com.fepsa.remises.data_synchronization.service;


import com.fepsa.remises.ui.application.App;

import rx.Subscriber;

/**
 * Created by nicolas.martinez on 11/5/2017.
 */

public class OnDemanUploadTripService extends BaseOnDemandSyncService<Boolean> {

    protected static final String ON_DEMAND_UPLOAD_TRIP_SERVICE_LAST_SUCCESSFUL_CALL_TIME =
            "ON_DEMAND_UPLOAD_TRIP_SERVICE_LAST_SUCCESSFUL_CALL_TIME";

    protected static final String LOG_TAG = "ON_DEMAND_UPLOAD_TRIP_SERVICE";

    public OnDemanUploadTripService() {

        super(OnDemanUploadTripService.class.getName(),
                LOG_TAG,
                ON_DEMAND_UPLOAD_TRIP_SERVICE_LAST_SUCCESSFUL_CALL_TIME
        );

    }

    @Override
    protected void syncData(Subscriber<Boolean> subscriber) {

        App.businessManager.uploadTrips().subscribe(subscriber);

    }

    @Override
    protected void onSyncFail(Throwable e) {

    }

    @Override
    protected void onSyncSuccess(Boolean result) {



    }

}
