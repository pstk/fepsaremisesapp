package com.fepsa.remises.data_synchronization.service;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.WorkerThread;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;

/**
 * Created by tksko on 02/07/2017.
 */
public abstract class NonStopIntentService extends JobService {

    private String mName;
    private volatile Looper mServiceLooper;
    private volatile ServiceHandler mServiceHandler;

    private static boolean isRunning;

    public NonStopIntentService(String name) {
        super();
        mName = name;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        HandlerThread thread = new HandlerThread("IntentService[" + mName + "]");
        thread.start();

        mServiceLooper = thread.getLooper();

        mServiceHandler = new ServiceHandler(mServiceLooper);
    }

    @Override
    public boolean onStartJob(JobParameters jobParameters) {

        Message msg = mServiceHandler.obtainMessage();

        msg.obj = jobParameters;

        mServiceHandler.sendMessage(msg);

        return false;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        return false;
    }

    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            isRunning = true;

            onHandleJobParameters((JobParameters) msg.obj);
        }
    }

    protected void stopRunning() {

        isRunning = false;
        stopSelf();
    }

    public static boolean isRunning() {

        return isRunning;
    }

    @Override
    public void onDestroy() {
        mServiceLooper.quit();
    }

    @WorkerThread
    protected abstract void onHandleJobParameters(JobParameters params);

}
