package com.fepsa.remises.ui.util;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.RectShape;
import android.os.Build;
import android.support.annotation.ColorRes;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.widget.ProgressBar;

import com.fepsa.remises.R;


/**
 * Created by tksko 06/07/2017.
 */

/**
 * Created by tksko on 31/10/02017.
 */

public class GraphicUtils {

    public static void tintProgressBar(ProgressBar progressBar, @ColorRes @Nullable Integer colorResId) {

        if (colorResId == null) {
            colorResId = R.color.colorAccent;
        }

        int color = ContextCompat.getColor(progressBar.getContext(), colorResId);

        // Progress Bar tinting
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {

            Drawable wrapDrawable = DrawableCompat.wrap(progressBar.getIndeterminateDrawable());
            DrawableCompat.setTint(wrapDrawable, color);
            progressBar.setIndeterminateDrawable(DrawableCompat.unwrap(wrapDrawable));
        } else {
            progressBar.getIndeterminateDrawable().setColorFilter(color, PorterDuff.Mode.SRC_IN);
        }

    }

    public static Drawable getRoundedDrawable(int cornerRadius) {

        GradientDrawable roundedDrawable = new GradientDrawable();

        roundedDrawable.setColor(Color.WHITE);

        roundedDrawable.setCornerRadius(ViewUtils.dpToPx(cornerRadius));

        return roundedDrawable;
    }

    public static ColorStateList getColorStateList(int colorEnabled, int colorPressed, int colorDisabled) {

        int[][] states = new int[][]{
                new int[]{-android.R.attr.state_enabled}, // disabled
                new int[]{android.R.attr.state_pressed}, // pressed
                new int[]{android.R.attr.state_enabled} // enabled
        };

        int[] colors = new int[]{
                colorDisabled, // disabled
                colorPressed, // pressed
                colorEnabled // enabled
        };

        return new ColorStateList(states, colors);
    }

    public static Drawable getCircularBackground(int color) {

        return getBackground(color, true);
    }

    public static Drawable getRectBackground(int color) {

        return getBackground(color, false);
    }

    private static Drawable getBackground(int color, boolean isOval) {

        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
                ? getRippleDrawable(color)
                : getSelectorDrawable(color, isOval);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private static RippleDrawable getRippleDrawable(int pressedColor) {

        return new RippleDrawable(getPressedColorSelector(pressedColor), null, null);
    }

    private static ColorStateList getPressedColorSelector(int pressedColor) {

        return new ColorStateList(
                new int[][]
                        {
                                new int[]{}
                        },
                new int[]
                        {
                                pressedColor
                        }
        );
    }

    private static StateListDrawable getSelectorDrawable(int color, boolean isOval) {

        StateListDrawable res = new StateListDrawable();

        res.addState(new int[]{android.R.attr.state_pressed}, getShapeDrawable(color, isOval));

        res.addState(new int[]{}, new ColorDrawable(Color.TRANSPARENT));

        return res;
    }

    private static Drawable getShapeDrawable(int color, boolean isOval) {

        ShapeDrawable drawable = new ShapeDrawable(isOval ? new OvalShape() : new RectShape());

        drawable.getPaint().setColor(color);

        return drawable;
    }

}
