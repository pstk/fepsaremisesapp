package com.fepsa.remises.ui.fragment.view;

/**
 * Created by tksko on 4/12/2017.
 */

public interface LoginView extends BaseView {

    void onSuccessLogin();

}
