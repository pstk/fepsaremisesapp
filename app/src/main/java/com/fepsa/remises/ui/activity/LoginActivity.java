package com.fepsa.remises.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.fepsa.remises.ui.fragment.LoginFragment;
import com.fepsa.remises.util.AuthUtils;

/**
 * Created by tksko on 02/07/2017.
 */

public class LoginActivity extends BaseActivity {

    public static Intent getCallingIntent(Context context) {

        Intent intent = new Intent(context, LoginActivity.class);

        Bundle bundle = new Bundle();
        intent.putExtras(bundle);

        return intent;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if (!AuthUtils.isLoggedIn()) {

            replaceFragment(LoginFragment.newInstance());

            if (getSupportActionBar() != null) {

                getSupportActionBar().hide();
            }

        } else {

            startActivity(HomeActivity.getCallingIntent(this, false));

            finish();

        }

    }

}