package com.fepsa.remises.ui.adapter.recyler_view_adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fepsa.remises.R;
import com.fepsa.remises.ui.adapter.holder.EmptyViewHolder;

import java.util.List;

/**
 * Created by tksko on 02/07/2017.
 */
public class EmptyAdapter extends RecyclerView.Adapter<EmptyViewHolder> {

    private List<Object> list;

    private int itemLayout;

    private int selected;

    public EmptyAdapter(List<Object> list) {

        this.list = list;
        this.selected = -1;
    }

    @Override
    public EmptyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_empty, parent, false);

        return new EmptyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final EmptyViewHolder holder, final int position) {

        final Object item = list.get(position);

        if (item != null) {

        }

    }

    @Override
    public int getItemCount() {

        return list == null ? 0 : list.size();
    }

    public boolean isEmpty() {

        return getItemCount() == 0;
    }

    public void updateList(List<Object> list) {

        int count = this.list.size();

        this.list = list;
        this.selected = -1;

        this.notifyItemRangeRemoved(0, count);

        this.notifyItemRangeInserted(0, this.list.size());
    }

    private void onCardClick(Object item) {

    }

    public void clearSelection() {

        if (selected != -1) {
            int prevSelected = selected;
            this.selected = -1;
            this.notifyItemChanged(prevSelected);
        }

    }

}