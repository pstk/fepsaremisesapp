package com.fepsa.remises.ui.util;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;

import com.fepsa.remises.R;
import com.fepsa.remises.ui.application.App;

/**
 * Created by tksko on 31/10/02017.
 */

public class ColorUtils {

    public static int getColorFromHexaString(Context context, String hexaColor) {

        int color = ContextCompat.getColor(App.applicationContext, R.color.black);

        try {

            if (!android.text.TextUtils.isEmpty(hexaColor))
                color = Color.parseColor(hexaColor);

        } catch (Exception e) {
        }

        return color;

    }

    /**
     * @param color
     * @param opacity range: [0, 1]
     * @return
     */
    public static int applyAlphaToColor(int color, float opacity) {

        int alpha = Math.round(Color.alpha(color) * opacity);

        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);

        return Color.argb(alpha, red, green, blue);
    }

    /**
     * Lightens a color by the specified fraction.
     */
    public static int lightenColor(int color, double fraction) {

        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);

        red = lightenColorChannel(red, fraction);
        green = lightenColorChannel(green, fraction);
        blue = lightenColorChannel(blue, fraction);

        int alpha = Color.alpha(color);

        return Color.argb(alpha, red, green, blue);
    }

    /**
     * Darkens a color by the specified fraction.
     */
    public static int darkenColor(int color, double fraction) {

        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);

        red = darkenColorChannel(red, fraction);
        green = darkenColorChannel(green, fraction);
        blue = darkenColorChannel(blue, fraction);

        int alpha = Color.alpha(color);

        return Color.argb(alpha, red, green, blue);
    }

    private static int darkenColorChannel(int color, double fraction) {
        return (int) Math.max(color - (color * fraction), 0);
    }

    private static int lightenColorChannel(int color, double fraction) {
        return (int) Math.min(color + (color * fraction), 255);
    }

}
