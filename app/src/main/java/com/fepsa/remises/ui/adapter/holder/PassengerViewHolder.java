package com.fepsa.remises.ui.adapter.holder;

import android.content.res.ColorStateList;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fepsa.remises.R;
import com.fepsa.remises.ui.application.App;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tksko on 02/07/2017.
 */

public class PassengerViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.list_item_passenger_main_layout)
    public LinearLayout mainLayout;

    @BindView(R.id.list_item_passenger_name)
    public TextView nameView;

    @BindView(R.id.list_item_passenger_legajo)
    public TextView legajoView;

    @BindView(R.id.list_item_passenger_action)
    public FloatingActionButton action;

    public PassengerViewHolder(View itemView, boolean removeMode) {

        super(itemView);

        ButterKnife.bind(this, itemView);

        if (removeMode) {

            action.setImageResource(R.drawable.ic_minus);

            action.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(App.applicationContext, R.color.red)));

        } else {

            action.setImageResource(R.drawable.ic_plus);

            action.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(App.applicationContext, R.color.green)));

        }

    }

}
