package com.fepsa.remises.ui.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.fepsa.remises.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tksko on 02/07/2017.
 */

public class GpsDataViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.date)
    public TextView date;

    @BindView(R.id.speed)
    public TextView speed;

    @BindView(R.id.bearing)
    public TextView bearing;

    @BindView(R.id.accuracy)
    public TextView accuracy;

    @BindView(R.id.extaInfo)
    public TextView extraInfo;

    public GpsDataViewHolder(View itemView) {

        super(itemView);

        ButterKnife.bind(this, itemView);

    }

}
