package com.fepsa.remises.ui.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

/**
 * Created by tksko on 02/07/2017.
 */

public class EmptyViewHolder extends RecyclerView.ViewHolder {

    public EmptyViewHolder(View itemView) {

        super(itemView);

        ButterKnife.bind(this, itemView);

    }

}
