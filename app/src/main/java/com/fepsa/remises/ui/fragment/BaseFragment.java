package com.fepsa.remises.ui.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import com.fepsa.remises.ui.fragment.presenter.BasePresenter;
import com.fepsa.remises.ui.util.GpsUtils;
import com.fepsa.remises.util.ParametrosUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import butterknife.Unbinder;

/**
 * Created by tksko on 02/07/2017.
 */

public class BaseFragment<Presenter extends BasePresenter> extends Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final int REQUEST_LOCATION_PERMISSIONS = 500;

    protected Presenter presenter;

    protected Unbinder bkUnbinder; // = ButterKnife.bind(this, view); onCreateView...

    private GoogleApiClient mGoogleApiClient;

    private LocationRequest mLocationRequest;

    private FusedLocationProviderApi mLocationProvider;

    private ProgressDialog progressDialog;

    private Location lastKnownLocation;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);


        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mLocationProvider = LocationServices.FusedLocationApi;

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(ParametrosUtils.getInstance().getGPSInterval());
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    }

    @Override
    public void onResume() {

        super.onResume();

        if (!GpsUtils.isGpsON(getActivity()))
            GpsUtils.showGPSAlert(getActivity());

        if (mGoogleApiClient != null) {

            if (!mGoogleApiClient.isConnected())
                mGoogleApiClient.connect();
            else if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        REQUEST_LOCATION_PERMISSIONS);

            } else {

                mLocationProvider.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

            }

        }

        if (presenter != null) {
            presenter.onResume();
        }

    }

    @Override
    public void onPause() {

        super.onPause();

        if (mLocationProvider != null && mGoogleApiClient != null) {

            if (mGoogleApiClient.isConnected())
                mLocationProvider.removeLocationUpdates(mGoogleApiClient, this);

        }

        if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();

        if (presenter != null) {
            presenter.onPause();
        }
    }

    @Override
    public void onDestroyView() {

        super.onDestroyView();

        if (bkUnbinder != null) {
            bkUnbinder.unbind();
        }

        if (presenter != null) {
            presenter.onDestroy();
        }

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_LOCATION_PERMISSIONS);

        } else {

            mLocationProvider.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public Location getLastKnownLocation() {

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (lastKnownLocation != null)
                return lastKnownLocation;
            else
                return new Location("");

        }

        Location theLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);

        if (theLocation != null) {

            if (lastKnownLocation != null) {

                if (lastKnownLocation.getTime() > theLocation.getTime())
                    return lastKnownLocation;
                else
                    return theLocation;

            } else {

                return theLocation;

            }

        } else {

            if (lastKnownLocation != null)
                return lastKnownLocation;
            else
                return new Location("");

        }

    }

    public void showProgressDialog() {

        progressDialog = new ProgressDialog(getActivity());

        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        progressDialog.setMessage("Procesando...");

        progressDialog.setCancelable(false);

        progressDialog.setIndeterminate(true);

        progressDialog.show();

    }

    public void hideProgressDialog() {

        if (progressDialog != null)
            progressDialog.dismiss();

    }

    @Override
    public void onLocationChanged(Location location) {

        lastKnownLocation = location;

    }

}
