package com.fepsa.remises.ui.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.transition.AutoTransition;

import com.fepsa.remises.R;
import com.fepsa.remises.ui.activity.presenter.BasePresenter;
import com.fepsa.remises.ui.fragment.BaseFragment;

/**
 * Created by tksko on 02/07/2017.
 */

public class BaseActivity<Presenter extends BasePresenter> extends AppCompatActivity {

    protected Presenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            getWindow().requestFeature(android.view.Window.FEATURE_CONTENT_TRANSITIONS);
            getWindow().requestFeature(android.view.Window.FEATURE_ACTIVITY_TRANSITIONS);

            getWindow().setEnterTransition(new AutoTransition());
            getWindow().setExitTransition(new AutoTransition());
            getWindow().setReturnTransition(new AutoTransition());
            getWindow().setReenterTransition(new AutoTransition());

            getWindow().setSharedElementEnterTransition(new AutoTransition());
            getWindow().setSharedElementExitTransition(new AutoTransition());
            getWindow().setSharedElementReenterTransition(new AutoTransition());
            getWindow().setSharedElementReturnTransition(new AutoTransition());
        }

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_base);
    }

    @Override
    public void onResume() {

        super.onResume();

        if (presenter != null) {
            presenter.onResume();
        }

    }

    @Override
    public void onPause() {

        super.onPause();

        if (presenter != null) {
            presenter.onPause();
        }

    }

    @Override
    public void onDestroy() {

        super.onDestroy();

        if (presenter != null) {
            presenter.onDestroy();
        }

    }

    public void replaceFragment(int containerId, BaseFragment fragment, int enterAnimation, int exitAnimation) {

        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(enterAnimation, exitAnimation)
                .replace(containerId, fragment)
                .commit();
    }

    public void replaceFragment(int containerId, BaseFragment fragment) {

        getSupportFragmentManager()
                .beginTransaction()
                .replace(containerId, fragment)
                .commit();
    }

    public void replaceFragment(BaseFragment fragment) {

        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .replace(R.id.activity_base_fragment_container, fragment)
                .commit();

    }

    public void replaceFragmentNow(BaseFragment fragment) {

        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .replace(R.id.activity_base_fragment_container, fragment)
                .commitNow();

    }

}