package com.fepsa.remises.ui.application;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;

import com.crashlytics.android.Crashlytics;
import com.facebook.stetho.Stetho;
import com.fepsa.remises.BuildConfig;
import com.fepsa.remises.business.BusinessManager;
import com.fepsa.remises.data_synchronization.DataSynchronizationManager;
import com.fepsa.remises.util.AuthUtils;
import com.fepsa.remises.util.SharedPrefs;
import com.uphyca.stetho_realm.RealmInspectorModulesProvider;

import io.fabric.sdk.android.Fabric;

/**
 * Created by tksko on 02/07/2017.
 */

public class App extends Application {

    public static BusinessManager businessManager;

    public static Context applicationContext;

    @Override
    public void onCreate() {

        super.onCreate();

        applicationContext = this;

        SharedPrefs.init(this);

        // One purge per app. Runs only if no active trip nor pending trips to upload
        SharedPrefs.saveBoolean(SharedPrefs.Constants.PURGE_DB, true);

        businessManager = new BusinessManager();

        Fabric.with(this, new Crashlytics());

        if (AuthUtils.isLoggedIn())
            DataSynchronizationManager.initialise(this);

        final Thread.UncaughtExceptionHandler ueh = Thread.getDefaultUncaughtExceptionHandler();

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {

            @Override
            public void uncaughtException(Thread thread, Throwable ex) {

                try {

                    Crashlytics.logException(ex);

                } catch (Exception e) {
                    // the crash couldn't be reported. do nothing
                }

                ueh.uncaughtException(thread, ex);

            }

        });

        if (BuildConfig.DEBUG) {

            Stetho.initialize(
                    Stetho.newInitializerBuilder(this)
                            .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                            .enableWebKitInspector(RealmInspectorModulesProvider
                                    .builder(this)
                                    .withLimit(1000000)
                                    .withDeleteIfMigrationNeeded(true)
                                    .build())
                            .build());
        }

    }


    public static String getVersionName() {
        try {
            return applicationContext.getPackageManager().getPackageInfo(
                    applicationContext.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e1) {
            return "";
        }
    }

    public static int getVersionCode() {
        try {
            return applicationContext.getPackageManager().getPackageInfo(
                    applicationContext.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e1) {
            return 0;
        }
    }

}
