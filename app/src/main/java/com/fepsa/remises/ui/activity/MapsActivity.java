package com.fepsa.remises.ui.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.fepsa.remises.R;
import com.fepsa.remises.business.data.database.model.RealmLocation;
import com.fepsa.remises.business.data.database.model.RealmPassenger;
import com.fepsa.remises.business.data.database.model.RealmTrip;
import com.fepsa.remises.ui.application.App;
import com.fepsa.remises.ui.util.BaseFunctions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscriber;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, AdapterView.OnItemSelectedListener {

    public static final double bahia_lat = -38.717702;

    public static final double bahia_long = -62.265705;

    private GoogleMap mMap;

    @BindView(R.id.spinner)
    Spinner tripsSpinner;

    @BindView(R.id.button_gps)
    Button gpsData;

    List<RealmTrip> trips;

    int selectedTrip = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_maps);

        ButterKnife.bind(this);

        gpsData.setVisibility(View.GONE);

        tripsSpinner.setOnItemSelectedListener(this);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng bahiaBlanca = new LatLng(bahia_lat, bahia_long);
        mMap.addMarker(new MarkerOptions().position(bahiaBlanca).title("Bahía Blanca"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(bahiaBlanca, 14));

        drawPath();

    }

    private void drawPath() {

        App.businessManager.getDatabaseManager().getPendingTrips().subscribe(
                new Subscriber<List<RealmTrip>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<RealmTrip> pendingTrips) {

                        trips = pendingTrips;

                        ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), R.layout.list_item_map_trip);

                        for (RealmTrip realmTrip : pendingTrips) {
                            adapter.add(new String(BaseFunctions.formatDate(realmTrip.getStartDate())));
                        }

                        tripsSpinner.setAdapter(adapter);

                    }

                }
        );

    }

    @OnClick(R.id.button_gps)
    public void onGpsClick() {

        startActivity(GpsDetailActivity.getCallingIntent(this, trips.get(selectedTrip).getStartDate()));

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (mMap != null) {
            mMap.clear();
        }

        gpsData.setVisibility(View.GONE);

        if (trips.size() > 0) {

            selectedTrip = position;

            gpsData.setVisibility(View.VISIBLE);

            int color = Color.argb(100, 255, 0, 0);

            final PolylineOptions result = new PolylineOptions().width(6).color(color);

            String path = "";

            RealmTrip theTrip = trips.get(position);

            for (RealmLocation l : theTrip.getLocations()) {
                //     path += "|" + l.getLatitude() + "," + l.getLongitude();
                result.add(new LatLng(l.getLatitude(), l.getLongitude()));
            }

                        /*
                        if (path.length() > 1) {

                            BackendManager.getInstance().snapToRoad(path.substring(1)).subscribe(new Subscriber<SnapToRoadResponse>() {
                                @Override
                                public void onCompleted() {

                                }

                                @Override
                                public void onError(Throwable e) {

                                    e.printStackTrace();
                                }

                                @Override
                                public void onNext(SnapToRoadResponse snapToRoadResponse) {

                                    for (SnappedPointEntity entity : snapToRoadResponse.getSnappedPoints()) {
                                        result.add(
                                                new LatLng(
                                                        entity.getLocation().getLatitude(),
                                                        entity.getLocation().getLongitude()
                                                )
                                        );
                                    }

                                    mMap.addPolyline(result);

                                    if (result.getPoints().size() > 0) {
                                        mMap.moveCamera(CameraUpdateFactory.newLatLng(result.getPoints().get(0)));
                                    }

                                }
                            });

                        }
*/

            mMap.addPolyline(result);


            // Passengers Markers

            for (RealmPassenger passenger : theTrip.getPassengers()) {

                mMap.addMarker(new MarkerOptions().position(getGooglePos(passenger.getPosSube()))
                        .title("Sube: " + passenger.getPerson().getName()));

                mMap.addMarker(new MarkerOptions()
                        .position(getGooglePos(passenger.getPosBaja()))
                        .title("Baja: " + passenger.getPerson().getName())
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));

            }

            if (result.getPoints().size() > 0) {
                mMap.moveCamera(CameraUpdateFactory.newLatLng(result.getPoints().get(0)));
            }

        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

        gpsData.setVisibility(View.GONE);

        selectedTrip = -1;

    }

    private LatLng getGooglePos(RealmLocation location) {

        return new LatLng(location.getLatitude(), location.getLongitude());
    }
}
