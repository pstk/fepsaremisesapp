package com.fepsa.remises.ui.activity.presenter;

import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.fepsa.remises.business.data.database.model.RealmTrip;
import com.fepsa.remises.data_synchronization.service.OnDemanUploadTripService;
import com.fepsa.remises.ui.activity.view.TripActivityView;
import com.fepsa.remises.ui.application.App;
import com.fepsa.remises.ui.model.TripDTO;
import com.fepsa.remises.util.ParametrosUtils;
import com.fepsa.remises.util.SharedPrefs;
import com.fepsa.remises.util.TripUtils;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;

import rx.Subscriber;

/**
 * Created by nicolas.m on 11/3/2017.
 */

public class TripActivityPresenter extends BasePresenter<TripActivityView> {

    public TripActivityPresenter() {

    }

    public void getActiveTrip() {

        addSubscription(App.businessManager.getDatabaseManager().getActiveTrip().subscribe(new Subscriber<RealmTrip>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        Crashlytics.logException(e);

                        view.onError(e.getMessage());

                    }

                    @Override
                    public void onNext(RealmTrip realmTrip) {

                        view.onSuccessGetTrip(realmTrip != null ? new TripDTO(realmTrip) : null);

                    }

                })
        );

    }

    public void purgeDB() {

        if (!TripUtils.getInstance().isActiveTrip() && SharedPrefs.fetchBoolean(SharedPrefs.Constants.PURGE_DB, false)) {

            addSubscription(
                    App.businessManager.getDatabaseManager().cleanPersons(ParametrosUtils.getInstance().getVersionPersonas()).subscribe(new Subscriber<Boolean>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {

                            Crashlytics.logException(e);

                        }

                        @Override
                        public void onNext(Boolean result) {

                            SharedPrefs.saveBoolean(SharedPrefs.Constants.PURGE_DB, false);

                        }
                    })
            );
        }

    }

    public void uploadTrips(Context context) {

        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(context));

        if (!OnDemanUploadTripService.isRunning()) {

            dispatcher.mustSchedule(dispatcher.newJobBuilder()
                    .setService(OnDemanUploadTripService.class) // the JobService that will be called
                    .setTag(OnDemanUploadTripService.class.getSimpleName())        // uniquely identifies the job
                    .build());

        }

    }

}
