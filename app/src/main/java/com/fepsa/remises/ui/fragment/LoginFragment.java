package com.fepsa.remises.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.fepsa.remises.R;
import com.fepsa.remises.ui.activity.HomeActivity;
import com.fepsa.remises.ui.application.App;
import com.fepsa.remises.ui.fragment.presenter.LoginPresenter;
import com.fepsa.remises.ui.fragment.view.LoginView;
import com.fepsa.remises.ui.util.BaseFunctions;
import com.fepsa.remises.ui.util.ColorUtils;
import com.fepsa.remises.ui.util.GraphicUtils;
import com.fepsa.remises.ui.util.ViewUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;

/**
 * Created by nicolas.m on 10/31/2017.
 */

public class LoginFragment extends BaseFragment<LoginPresenter> implements LoginView {

    @BindView(R.id.fragment_login_login_button)
    AppCompatButton buttonSend;

    @BindView(R.id.fragment_login_edit_text_activation_code_layout)
    TextInputLayout editTextActivationLayout;

    @BindView(R.id.fragment_login_edit_text_activation_code)
    TextInputEditText editTextActivation;

    @BindView(R.id.fragment_login_edit_text_name_layout)
    TextInputLayout editTextNameLayout;

    @BindView(R.id.fragment_login_edit_text_name)
    TextInputEditText editTextName;

    @BindView(R.id.fragment_login_edit_text_dni_layout)
    TextInputLayout editTextDNILayout;

    @BindView(R.id.fragment_login_edit_text_dni)
    TextInputEditText editTextDNI;

    public LoginFragment() {
        // Required empty public constructor
    }

    public static LoginFragment newInstance() {

        LoginFragment fragment = new LoginFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new LoginPresenter();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_login, container, false);

        bkUnbinder = ButterKnife.bind(this, view);

        updateButton();

        setupFields();

        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        presenter.setView(this, this);

    }

    private void setupFields() {

        editTextName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            public void afterTextChanged(Editable edt) {
                if (editTextName.getText().length() > 0) {
                    editTextNameLayout.setError(null);
                }
            }
        });

        editTextActivation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            public void afterTextChanged(Editable edt) {
                if (editTextActivation.getText().length() > 0) {
                    editTextActivationLayout.setError(null);
                }
            }
        });

        editTextDNI.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            public void afterTextChanged(Editable edt) {
                if (editTextDNI.getText().length() > 0) {
                    editTextDNILayout.setError(null);
                }
            }
        });

    }

    private void updateButton() {

        buttonSend.setEnabled(true);

        int color = ContextCompat.getColor(App.applicationContext, R.color.colorPrimary);

        int colorEnabled = color;

        int colorPressed = ColorUtils.darkenColor(color, 0.20d);

        int colorDisabled = ContextCompat.getColor(App.applicationContext, R.color.button_disabled);

        buttonSend.setSupportBackgroundTintList(GraphicUtils.getColorStateList(
                colorEnabled,
                colorPressed,
                colorDisabled
        ));

    }

    @OnClick(R.id.fragment_login_login_button)
    public void onConfirmClick() {

        if (validData()) {

            showProgressDialog();

            ViewUtils.hideKeyboard(getActivity());

            presenter.loginChofer(editTextName.getText().toString().trim(),
                    editTextActivation.getText().toString().trim(),
                    editTextDNI.getText().toString().trim()
            );

        }

    }

    private boolean validData() {

        if (editTextActivation.getText().toString().trim().length() == 0) {
            editTextActivationLayout.setError(getString(R.string.fragment_login_invalid_data));
            return false;
        } else {
            editTextActivationLayout.setError(null);
        }

        if (editTextName.getText().toString().trim().length() == 0) {
            editTextNameLayout.setError(getString(R.string.fragment_login_invalid_data));
            return false;
        } else {
            editTextNameLayout.setError(null);
        }

        if (editTextDNI.getText().toString().trim().length() == 0) {
            editTextDNILayout.setError(getString(R.string.fragment_login_invalid_data));
            return false;
        } else {
            editTextDNILayout.setError(null);
        }

        return true;

    }

    @OnEditorAction(R.id.fragment_login_edit_text_dni)
    boolean onConfirmAction(TextView v, int actionId, KeyEvent event) {

        if (actionId == EditorInfo.IME_ACTION_DONE) {

            onConfirmClick();

            return true;

        }

        return false;

    }

    @Override
    public void onError(String message) {

        hideProgressDialog();

        BaseFunctions.displaySnackbar(getView(), message);

    }

    @Override
    public void onSuccessLogin() {

        hideProgressDialog();

        startActivity(HomeActivity.getCallingIntent(getActivity(), true));

        getActivity().finish();

    }

}
