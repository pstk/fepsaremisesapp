package com.fepsa.remises.ui.activity.view;

import com.fepsa.remises.ui.model.TripDTO;

/**
 * Created by tksko on 02/07/2017.
 */

public interface TripActivityView extends BaseView {

    void onSuccessGetTrip(TripDTO trip);

}