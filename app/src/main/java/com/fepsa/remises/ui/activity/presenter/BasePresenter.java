package com.fepsa.remises.ui.activity.presenter;

import com.fepsa.remises.ui.activity.view.BaseView;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by tksko on 02/07/2017.
 */

public class BasePresenter<View extends BaseView> implements Presenter {

    protected View view;

    private CompositeSubscription subscriptions;

    public BasePresenter() {

        this.subscriptions = new CompositeSubscription();
    }

    public void setView(View view) {

        this.view = view;
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onDestroy() {

        cancelAllSubscriptions();

        this.view = null;

    }

    protected void addSubscription(Subscription subscription) {

        subscriptions.add(subscription);
    }

    private void cancelAllSubscriptions() {

        subscriptions.unsubscribe();
    }

}