package com.fepsa.remises.ui.fragment;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.fepsa.remises.BuildConfig;
import com.fepsa.remises.R;
import com.fepsa.remises.business.data.database.model.RealmTrip;
import com.fepsa.remises.config.AppConstants;
import com.fepsa.remises.data_synchronization.service.PendingTripsSyncService;
import com.fepsa.remises.ui.activity.HomeActivity;
import com.fepsa.remises.ui.activity.MapsActivity;
import com.fepsa.remises.ui.application.App;
import com.fepsa.remises.ui.fragment.presenter.TripFragmentPresenter;
import com.fepsa.remises.ui.fragment.view.TripFragmentView;
import com.fepsa.remises.ui.model.TripDTO;
import com.fepsa.remises.ui.util.BaseFunctions;
import com.fepsa.remises.ui.util.ColorUtils;
import com.fepsa.remises.ui.util.GpsUtils;
import com.fepsa.remises.ui.util.GraphicUtils;
import com.fepsa.remises.util.TripUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscriber;

/**
 * Created by nicolas.m on 10/31/2017.
 */

public class HomeFragment extends BaseFragment<TripFragmentPresenter> implements TripFragmentView {

    private static final int REQUEST_PERMISSIONS = 200;

    @BindView(R.id.fragment_home_button_start)
    AppCompatButton buttonStart;

    @BindView(R.id.fragment_home_button_map)
    AppCompatButton buttonMap;

    @BindView(R.id.fragment_home_button_clear)
    AppCompatButton buttonClear;

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance() {

        HomeFragment fragment = new HomeFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new TripFragmentPresenter();

        //   ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        //   ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(false);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);

        bkUnbinder = ButterKnife.bind(this, view);

        updateButtonStart();

        updateButtonMap();

        updateButtonClear();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        presenter.setView(this, this);

    }

    private void updateButtonStart() {

        buttonStart.setEnabled(true);

        int color = ContextCompat.getColor(App.applicationContext, R.color.green);

        int colorEnabled = color;

        int colorPressed = ColorUtils.darkenColor(color, 0.20d);

        int colorDisabled = ContextCompat.getColor(App.applicationContext, R.color.button_disabled);

        buttonStart.setSupportBackgroundTintList(GraphicUtils.getColorStateList(
                colorEnabled,
                colorPressed,
                colorDisabled
        ));

    }

    private void updateButtonMap() {

        buttonMap.setVisibility(BuildConfig.DEBUG ? View.VISIBLE : View.GONE);

        buttonMap.setEnabled(true);

        int color = ContextCompat.getColor(App.applicationContext, R.color.orange);

        int colorEnabled = color;

        int colorPressed = ColorUtils.darkenColor(color, 0.20d);

        int colorDisabled = ContextCompat.getColor(App.applicationContext, R.color.button_disabled);

        buttonMap.setSupportBackgroundTintList(GraphicUtils.getColorStateList(
                colorEnabled,
                colorPressed,
                colorDisabled
        ));

    }

    private void updateButtonClear() {

        buttonClear.setVisibility(BuildConfig.DEBUG ? View.VISIBLE : View.GONE);

        buttonClear.setEnabled(true);

        int color = ContextCompat.getColor(App.applicationContext, R.color.red);

        int colorEnabled = color;

        int colorPressed = ColorUtils.darkenColor(color, 0.20d);

        int colorDisabled = ContextCompat.getColor(App.applicationContext, R.color.button_disabled);

        buttonClear.setSupportBackgroundTintList(GraphicUtils.getColorStateList(
                colorEnabled,
                colorPressed,
                colorDisabled
        ));

    }

    @OnClick(R.id.fragment_home_button_start)
    public void onButtonStartClick() {

        buttonStart.setEnabled(false);

        checkLocationPermissions();

    }

    @OnClick(R.id.fragment_home_button_map)
    public void onButtonMapClick() {

        startActivity(new Intent(getActivity(), MapsActivity.class));

    }

    @Override
    public void onError(String message) {

        buttonStart.setEnabled(true);

        BaseFunctions.displaySnackbar(getView(), message);

    }

    @Override
    public void onSuccessGetTrip(TripDTO trip) {

    }

    @Override
    public void onSuccessCreateTrip(TripDTO trip) {

        buttonStart.setEnabled(true);

        TripUtils.getInstance().setActiveTripId(trip.getId());

        ((HomeActivity) getActivity()).navigateActiveTrip();

    }

    @Override
    public void onSuccessCloseTrip(TripDTO trip) {

    }

    @Override
    public void onSucessActionPassenger(Object person) {

    }

    @Override
    public void onErrorActionPassenger(Object person) {

    }

    @Override
    public void onSuccessGetPassengers(List passengers) {

    }

    @Override
    public void onSuccessRemoveTrip() {

    }

    @Override
    public void onSucessGetPersons(List persons) {

    }

    private void checkLocationPermissions() {

        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_PERMISSIONS);

        } else {

            startNewTrip();

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {

        if (REQUEST_PERMISSIONS == requestCode) {

            if (grantResults.length > 1
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                startNewTrip();

            }

        }

    }

    private void startNewTrip() {

        if (GpsUtils.isGpsON(getActivity()))
            presenter.newTrip(System.currentTimeMillis(), getLastKnownLocation());
        else
            GpsUtils.showGPSAlert(getActivity());

    }

    @OnClick(R.id.fragment_home_button_clear)
    public void onBorrarClick() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.fragment_trip_confirm_title);

        builder.setMessage(getString(R.string.fragment_home_confirm_clean_message));

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog

                cleanTrips();

                dialog.dismiss();

            }
        });

        builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();

        alert.show();


    }

    private void cleanTrips() {

        App.businessManager.getDatabaseManager().getPendingTrips().subscribe(new Subscriber<List<RealmTrip>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(List<RealmTrip> realmTrips) {

                for (RealmTrip trip : realmTrips) {

                    App.businessManager.getDatabaseManager().removeTrip(trip.getStartDate()).subscribe(new Subscriber<Boolean>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onNext(Boolean aBoolean) {

                        }
                    });

                }

                Intent pendingTrips = new Intent(AppConstants.BroadcastActions.PENDING_TRIPS);

                pendingTrips.putExtra("TRIPS_COUNT", 0);

                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(pendingTrips);

                Toast.makeText(getActivity(), "Viajes eliminados correctamente.", Toast.LENGTH_SHORT).show();

            }

        });

    }


    @Override
    public void onResume() {

        super.onResume();

        if (!PendingTripsSyncService.isRunning()) {

            Intent intent = new Intent(getActivity(), PendingTripsSyncService.class);

            getActivity().startService(intent);

        }

    }
}
