package com.fepsa.remises.ui.activity.view;

/**
 * Created by tksko on 02/07/2017.
 */

public interface BaseView {

    void onError(String message);

}