package com.fepsa.remises.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.fepsa.remises.R;
import com.fepsa.remises.business.data.database.model.RealmLocation;
import com.fepsa.remises.ui.adapter.recyler_view_adapter.GpsDataAdapter;
import com.fepsa.remises.ui.application.App;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscriber;

/**
 * Created by tksko on 02/07/2017.
 */

public class GpsDetailActivity extends BaseActivity {

    @BindView(R.id.activity_gps_details_recycler)
    RecyclerView recyclerView;

    private GpsDataAdapter adapter;

    private List<RealmLocation> data;

    public static Intent getCallingIntent(Context context, long tripId) {

        Intent intent = new Intent(context, GpsDetailActivity.class);

        Bundle bundle = new Bundle();
        bundle.putLong("TRIP_ID", tripId);
        intent.putExtras(bundle);

        return intent;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_gps_details);

        ButterKnife.bind(this);

        App.businessManager.getDatabaseManager().getTripLocations(getIntent().getLongExtra("TRIP_ID", 0)).subscribe(new Subscriber<List<RealmLocation>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(List<RealmLocation> realmLocations) {

                data = realmLocations;

                configRecyclerView();

            }
        });

    }

    private void configRecyclerView() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(layoutManager);

        adapter = new GpsDataAdapter(data);

        recyclerView.setAdapter(adapter);

        recyclerView.setHasFixedSize(true);

    }

}