package com.fepsa.remises.ui.adapter.recyler_view_adapter;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fepsa.remises.R;
import com.fepsa.remises.ui.adapter.holder.PassengerViewHolder;
import com.fepsa.remises.ui.application.App;
import com.fepsa.remises.ui.model.PersonDTO;
import com.fepsa.remises.ui.util.GpsUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tksko on 02/07/2017.
 */
public class PassengersAdapter extends RecyclerView.Adapter<PassengerViewHolder> {

    private List list;

    private long selected;

    private boolean removeMode = false;

    private PassengerActionListener listener;

    public PassengersAdapter(boolean removeMode, PassengerActionListener listener) {

        this.list = new ArrayList();
        this.selected = -1;
        this.removeMode = removeMode;
        this.listener = listener;
    }

    public PassengersAdapter(boolean removeMode) {

        this.list = new ArrayList();
        this.selected = -1;
        this.removeMode = removeMode;
    }

    public void setListener(PassengerActionListener listener) {
        this.listener = listener;
    }

    @Override
    public PassengerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_passenger, parent, false);

        return new PassengerViewHolder(itemView, removeMode);
    }

    @Override
    public void onBindViewHolder(final PassengerViewHolder holder, final int position) {

        final PersonDTO item = (PersonDTO) list.get(position);

        if (item != null) {

            holder.nameView.setText(item.getName());

            holder.legajoView.setVisibility((TextUtils.isEmpty(item.getLegajo()) || item.isLocal()) ? View.GONE : View.VISIBLE);

            if (!TextUtils.isEmpty(item.getLegajo())) {
                holder.legajoView.setText(item.getLegajo());
            }

            if (selected == item.getId()) {

                if (removeMode) {
                    holder.nameView.setTextColor(ContextCompat.getColor(App.applicationContext, R.color.red));
                } else {
                    holder.nameView.setTextColor(ContextCompat.getColor(App.applicationContext, R.color.green));
                }

                holder.action.setVisibility(View.VISIBLE);
            } else {

                holder.nameView.setTextColor(ContextCompat.getColor(App.applicationContext, R.color.black));

                holder.action.setVisibility(View.GONE);
            }

            holder.mainLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (selected == item.getId()) {
                        selected = -1;
                    } else {
                        selected = item.getId();
                    }

                    notifyDataSetChanged();

                }

            });

            holder.action.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (GpsUtils.isGpsON(holder.action.getContext())) {
                        selected = item.getId();

                        list.remove(position);

                        if (listener != null)
                            listener.onPassengerAction(item);

                        notifyDataSetChanged();
                    } else {
                        GpsUtils.showGPSAlert(holder.action.getContext());
                    }

                }

            });

        }

    }

    @Override
    public int getItemCount() {

        return list == null ? 0 : list.size();
    }

    public boolean isEmpty() {

        return getItemCount() == 0;
    }

    public void updateList(List list) {

        this.list = list;

        this.selected = -1;

        notifyDataSetChanged();

    }


    public interface PassengerActionListener {

        public void onPassengerAction(Object passenger);

    }

}