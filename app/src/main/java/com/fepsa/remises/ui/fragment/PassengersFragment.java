package com.fepsa.remises.ui.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.fepsa.remises.R;
import com.fepsa.remises.business.data.database.model.RealmPassenger;
import com.fepsa.remises.ui.adapter.recyler_view_adapter.PassengersAdapter;
import com.fepsa.remises.ui.fragment.presenter.TripFragmentPresenter;
import com.fepsa.remises.ui.fragment.view.TripFragmentView;
import com.fepsa.remises.ui.model.PersonDTO;
import com.fepsa.remises.ui.model.TripDTO;
import com.fepsa.remises.ui.util.BaseFunctions;
import com.fepsa.remises.ui.util.GpsUtils;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by nicolas.m on 10/31/2017.
 */

public class PassengersFragment extends BaseFragment<TripFragmentPresenter> implements PassengersAdapter.PassengerActionListener, TripFragmentView {

    @BindView(R.id.fragment_passenger_edit_text)
    EditText searchField;

    @BindView(R.id.fragment_passenger_recycler_view_results)
    RecyclerView resultsList;

    @BindView(R.id.fragment_passenger_recycler_swipe_view)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.fragment_passenger_empty_view)
    LinearLayout emptyView;

    private PassengersAdapter adapter;

    public PassengersFragment() {
        // Required empty public constructor
    }

    public static PassengersFragment newInstance() {

        PassengersFragment fragment = new PassengersFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        presenter = new TripFragmentPresenter();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_passenger, container, false);

        bkUnbinder = ButterKnife.bind(this, view);

        setupSearch();

        setupResults();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        presenter.setView(this, this);

        //Poner Foco en el Edit y abrir el Teclado
        searchField.requestFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(searchField, InputMethodManager.SHOW_IMPLICIT);

        presenter.searchPersons(null, false);

    }

    private void setupSearch() {

        searchField.addTextChangedListener(new TextWatcher() {

            private Timer timer;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                final String query = s.toString();

                if (timer != null)
                    timer.cancel();

                if (query.length() >= 2) {

                    timer = new Timer();
                    timer.schedule(new TimerTask() {

                        @Override
                        public void run() {

                            try {

                                getActivity().runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {

                                        presenter.searchPersons(query, false);
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, 400);

                } else if (query.length() == 0) {

                    presenter.searchPersons(null, false);
                }

            }
        });

    }

    private void setupResults() {

        adapter = new PassengersAdapter(false, this);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {

                swipeRefreshLayout.setRefreshing(true);

                presenter.searchPersons(null, true);

            }

        });

        resultsList.setAdapter(adapter);

        resultsList.setHasFixedSize(true);

        setEmptyViewVisibility();

    }

    private void setEmptyViewVisibility() {

        boolean visible = adapter.isEmpty();

        emptyView.setVisibility(visible ? View.VISIBLE : View.GONE);

        resultsList.setVisibility(visible ? View.GONE : View.VISIBLE);

    }

    @Override
    public void onPassengerAction(Object passenger) {

        PersonDTO person = (PersonDTO) passenger;

        if (GpsUtils.isGpsON(getActivity())) {
            presenter.addPassenger(person, getLastKnownLocation());
        } else {
            GpsUtils.showGPSAlert(getActivity());
        }

        setEmptyViewVisibility();

    }

    @OnClick(R.id.fragment_passenger_button_add_manually)
    public void onAddManuallyClick() {

        final String text = searchField.getText().toString();

        if (!TextUtils.isEmpty(text)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            builder.setTitle(R.string.fragment_trip_confirm_title);

            builder.setMessage(getString(R.string.fragment_trip_confirm_add_passanger_message) + text);

            builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    // Do nothing but close the dialog

                    onPassengerAction(new PersonDTO(System.currentTimeMillis(), text));

                    dialog.dismiss();

                }
            });

            builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                    // Do nothing
                    dialog.dismiss();
                }
            });

            AlertDialog alert = builder.create();

            alert.show();
        } else {

            BaseFunctions.displaySnackbar(getView(), getString(R.string.fragment_trip_error_empty_name));
        }

    }


    @Override
    public void onError(String message) {

        swipeRefreshLayout.setRefreshing(false);

        BaseFunctions.displaySnackbar(getView(), message);

    }

    @Override
    public void onSuccessGetTrip(TripDTO trip) {

    }

    @Override
    public void onSuccessCreateTrip(TripDTO trip) {

    }

    @Override
    public void onSuccessCloseTrip(TripDTO trip) {

    }

    @Override
    public void onSucessActionPassenger(Object person) {

        RealmPassenger passenger = (RealmPassenger) person;

        searchField.setText(null);

        BaseFunctions.displaySnackbar(getView(), "El pasajero " + passenger.getPerson().getName() + " fue agregado correctamente.");

    }

    @Override
    public void onErrorActionPassenger(Object person) {

        searchField.setText(null);

    }

    @Override
    public void onSuccessGetPassengers(List passengers) {

    }

    @Override
    public void onSuccessRemoveTrip() {

    }

    @Override
    public void onSucessGetPersons(List persons) {

        swipeRefreshLayout.setRefreshing(false);

        adapter.updateList(persons);

        setEmptyViewVisibility();

    }

}
