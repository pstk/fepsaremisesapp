package com.fepsa.remises.ui.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.fepsa.remises.R;
import com.fepsa.remises.ui.activity.HomeActivity;
import com.fepsa.remises.ui.application.App;
import com.fepsa.remises.util.ChannelHelper;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by tksko on 30/9/2017.
 */

public class NotificationUtil {

    public static final int RUNNING_NOTIFICATION_ID = 600;

    public static void showRunningNotification(Context context) {

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);

        notificationManager.notify(RUNNING_NOTIFICATION_ID, getNotification(context));

    }

    public static Notification getNotification(Context context) {

        Intent intent = new Intent(context, HomeActivity.class);

        PendingIntent pIntent = PendingIntent.getActivity(context, (int) System.currentTimeMillis(), intent, 0);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);

        String channelId = ChannelHelper.getInstance().createNotificationChannelDefault(notificationManager);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, channelId);

        builder.setSmallIcon(R.drawable.ic_notification_tksko)
                .setContentTitle(App.applicationContext.getString(R.string.app_name))
                .setContentText(App.applicationContext.getString(R.string.fragment_trip_header_message))
                .setContentIntent(pIntent)
                .setAutoCancel(false);

        Notification notification = builder.build();

        notification.flags |= Notification.FLAG_NO_CLEAR
                | Notification.FLAG_ONGOING_EVENT;

        return notification;

    }

    public static void hideRunningNotification(Context context) {

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancel(RUNNING_NOTIFICATION_ID);

    }

}
