package com.fepsa.remises.ui.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.fepsa.remises.R;
import com.fepsa.remises.business.services.FusedLocationService;
import com.fepsa.remises.ui.activity.HomeActivity;
import com.fepsa.remises.ui.application.App;
import com.fepsa.remises.ui.fragment.presenter.TripFragmentPresenter;
import com.fepsa.remises.ui.fragment.view.TripFragmentView;
import com.fepsa.remises.ui.model.TripDTO;
import com.fepsa.remises.ui.util.ColorUtils;
import com.fepsa.remises.ui.util.GpsUtils;
import com.fepsa.remises.ui.util.GraphicUtils;
import com.fepsa.remises.util.TripUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;

/**
 * Created by nicolas.m on 10/31/2017.
 */

public class CloseTripFragment extends BaseFragment<TripFragmentPresenter> implements TripFragmentView {

    @BindView(R.id.fragment_close_trip_end_button)
    AppCompatButton buttonEnd;

    @BindView(R.id.fragment_close_trip_edit_text_peajes)
    TextInputEditText editTextPeajes;

    @BindView(R.id.fragment_close_trip_edit_text_tiempo_espera)
    TextInputEditText editTextTiempoEspera;

    @BindView(R.id.fragment_close_trip_edit_text_nro_viaje)
    TextInputEditText editTextNroViaje;

    @BindView(R.id.fragment_close_trip_edit_text_ticket)
    TextInputEditText editTextTicket;

    public CloseTripFragment() {
        // Required empty public constructor
    }

    public static CloseTripFragment newInstance() {

        CloseTripFragment fragment = new CloseTripFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        presenter = new TripFragmentPresenter();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_close_trip, container, false);

        bkUnbinder = ButterKnife.bind(this, view);

        updateButton();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        presenter.setView(this, this);

        //Poner Foco en el primer edit y abrir el Teclado
        editTextNroViaje.requestFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editTextNroViaje, InputMethodManager.SHOW_IMPLICIT);

    }

    private void updateButton() {

        buttonEnd.setEnabled(true);

        int color = ContextCompat.getColor(App.applicationContext, R.color.blue);

        int colorEnabled = color;

        int colorPressed = ColorUtils.darkenColor(color, 0.20d);

        int colorDisabled = ContextCompat.getColor(App.applicationContext, R.color.button_disabled);

        buttonEnd.setSupportBackgroundTintList(GraphicUtils.getColorStateList(
                colorEnabled,
                colorPressed,
                colorDisabled
        ));

    }

    @OnClick(R.id.fragment_close_trip_end_button)
    public void onConfirmClick() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.fragment_trip_confirm_title);

        builder.setMessage(getString(R.string.fragment_trip_confirm_end_trip_message));

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog

                submitData();

                dialog.dismiss();

            }
        });

        builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();

        alert.show();

    }

    private void submitData() {

        buttonEnd.setEnabled(false);

        long nroViaje = 0;

        if (!TextUtils.isEmpty(editTextNroViaje.getText().toString())) {
            nroViaje = Long.parseLong(editTextNroViaje.getText().toString());
        }

        int minutes = 0;

        if (!TextUtils.isEmpty(editTextTiempoEspera.getText().toString())) {
            minutes = Integer.parseInt(editTextTiempoEspera.getText().toString());
        }

        double peajes = 0;

        if (!TextUtils.isEmpty(editTextPeajes.getText().toString())) {
            peajes = Double.parseDouble(editTextPeajes.getText().toString());
        }

        double ticket = 0;

        if (!TextUtils.isEmpty(editTextTicket.getText().toString())) {
            ticket = Double.parseDouble(editTextTicket.getText().toString());
        }

        if (GpsUtils.isGpsON(getActivity())) {
            presenter.closeTrip(nroViaje, minutes, peajes, ticket, getLastKnownLocation());
        } else {
            GpsUtils.showGPSAlert(getActivity());
        }

    }

    @OnEditorAction(R.id.fragment_close_trip_edit_text_peajes)
    boolean onConfirmAction(TextView v, int actionId, KeyEvent event) {

        if (actionId == EditorInfo.IME_ACTION_DONE) {

            onConfirmClick();

            return true;

        }

        return false;

    }

    @Override
    public void onError(String message) {

        buttonEnd.setEnabled(true);
    }

    @Override
    public void onSuccessGetTrip(TripDTO trip) {

    }

    @Override
    public void onSuccessCreateTrip(TripDTO trip) {

    }

    @Override
    public void onSuccessCloseTrip(TripDTO trip) {

        buttonEnd.setEnabled(true);

        getActivity().stopService(new Intent(getActivity(), FusedLocationService.class));

        TripUtils.getInstance().clearActiveTrip();

        presenter.uploadTrips(getActivity());

        ((HomeActivity) getActivity()).navigateHome();

    }

    @Override
    public void onSucessActionPassenger(Object person) {

    }

    @Override
    public void onErrorActionPassenger(Object person) {

    }

    @Override
    public void onSuccessGetPassengers(List passengers) {

    }

    @Override
    public void onSuccessRemoveTrip() {

    }

    @Override
    public void onSucessGetPersons(List persons) {

    }

}
