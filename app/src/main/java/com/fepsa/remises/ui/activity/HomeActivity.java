package com.fepsa.remises.ui.activity;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.fepsa.remises.R;
import com.fepsa.remises.config.AppConstants;
import com.fepsa.remises.data_synchronization.DataSynchronizationManager;
import com.fepsa.remises.ui.activity.presenter.TripActivityPresenter;
import com.fepsa.remises.ui.activity.view.TripActivityView;
import com.fepsa.remises.ui.application.App;
import com.fepsa.remises.ui.fragment.CloseTripFragment;
import com.fepsa.remises.ui.fragment.HomeFragment;
import com.fepsa.remises.ui.fragment.PassengersFragment;
import com.fepsa.remises.ui.fragment.TripFragment;
import com.fepsa.remises.ui.model.TripDTO;
import com.fepsa.remises.ui.util.ViewUtils;
import com.fepsa.remises.util.TripUtils;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;

/**
 * Created by tksko on 02/07/2017.
 */

public class HomeActivity extends BaseActivity<TripActivityPresenter> implements TripActivityView {

    private Toolbar toolbar;

    private MenuItem menuItemPending;

    private AlertDialog pendingTripsDialog = null;

    private boolean pendingDialogFirstView = true;

    public static Intent getCallingIntent(Context context, boolean afterLogin) {

        Intent intent = new Intent(context, HomeActivity.class);

        Bundle bundle = new Bundle();
        bundle.putBoolean("RUN_SYNC", afterLogin);
        intent.putExtras(bundle);

        return intent;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        toolbar = findViewById(R.id.activity_base_toolbar);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setDisplayShowHomeEnabled(true);

        presenter = new TripActivityPresenter();

        presenter.setView(this);

        if (getIntent().hasExtra("RUN_SYNC")) {

            boolean runService = getIntent().getBooleanExtra("RUN_SYNC", false);
            getIntent().removeExtra("RUN_SYNC");

            if (runService)
                DataSynchronizationManager.initialise(this);

        }

    }

    @Override
    public void onResume() {

        super.onResume();

        registerPendingTripsReceiver();

        presenter.getActiveTrip();

    }

    @Override
    public void onPause() {

        super.onPause();

        unregisterPendingTripsReceiver();
    }

    public void navigateHome() {

        ViewUtils.hideKeyboard(this);

        replaceFragment(HomeFragment.newInstance());

    }

    public void navigateActiveTrip() {

        ViewUtils.hideKeyboard(this);

        replaceFragment(TripFragment.newInstance());

    }

    public void navigateClsoeTrip() {

        ViewUtils.hideKeyboard(this);

        replaceFragment(CloseTripFragment.newInstance());

    }

    public void navigatePassengers() {

        ViewUtils.hideKeyboard(this);

        replaceFragment(PassengersFragment.newInstance());

    }

    @Override
    public void onBackPressed() {

        Fragment f = getSupportFragmentManager().findFragmentById(R.id.activity_base_fragment_container);

        if (f instanceof HomeFragment || f instanceof TripFragment) {
            super.onBackPressed();
        } else if (f instanceof CloseTripFragment || f instanceof PassengersFragment) {
            navigateActiveTrip();
        }

    }

    @Override
    public void onError(String message) {

        finish();

    }

    @Override
    public void onSuccessGetTrip(TripDTO trip) {

        if (trip == null) {

            navigateHome();

        } else {

            TripUtils.getInstance().setActiveTripId(trip.getId());

            navigateActiveTrip();

        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater findMenuItems = getMenuInflater();
        findMenuItems.inflate(R.menu.menu_home, menu);

        menuItemPending = menu.findItem(R.id.menu_pending);

        if (menuItemPending != null) {
            menuItemPending.setVisible(false);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_pending:
                Toast.makeText(this, R.string.msg_pending_trips, Toast.LENGTH_SHORT).show();
                break;
        }

        return super.onOptionsItemSelected(item);

    }

    // PENDING TRIPS

    BroadcastReceiver pendingTripsReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            int cant = intent.getIntExtra("TRIPS_COUNT", 0);

            if (menuItemPending != null) {

                menuItemPending.setVisible(cant > 0);

            }

            if (cant > 0) {

                if (!TripUtils.getInstance().isActiveTrip())
                    showTripsAlert(cant);

                presenter.uploadTrips(context);

            } else {

                presenter.purgeDB();

            }

        }

    };

    private void registerPendingTripsReceiver() {

        LocalBroadcastManager
                .getInstance(App.applicationContext)
                .registerReceiver(
                        pendingTripsReceiver,
                        new IntentFilter(AppConstants.BroadcastActions.PENDING_TRIPS)
                );

    }

    private void unregisterPendingTripsReceiver() {

        LocalBroadcastManager
                .getInstance(App.applicationContext)
                .unregisterReceiver(pendingTripsReceiver);

    }

    private void showTripsAlert(int cant) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.dialog_pending_trips_title, (cant == 1) ? "1 viaje" : cant + " viajes"))
                .setMessage(getString(R.string.dialog_pending_trips_message))
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        pendingDialogFirstView = false;
                        dialog.dismiss();
                    }
                });

        if (pendingDialogFirstView && (pendingTripsDialog == null || !pendingTripsDialog.isShowing())) {
            pendingTripsDialog = builder.create();
            pendingTripsDialog.show();
        }

    }

}