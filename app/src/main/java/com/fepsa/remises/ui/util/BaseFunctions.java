package com.fepsa.remises.ui.util;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fepsa.remises.R;
import com.fepsa.remises.config.AppConstants;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by tksko on 02/07/2017.
 */

public class BaseFunctions {

    private static ObjectMapper mapper;

    static {

        mapper = new ObjectMapper();

        mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);

        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);

        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);

        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        DateFormat WS_DATE_FORMAT = new SimpleDateFormat("MMM dd, yyyy h:mm:ss aa", Locale.ENGLISH);

        mapper.setDateFormat(WS_DATE_FORMAT);

    }

    public static ObjectMapper getMapper() {

        return mapper;
    }

    public static String getApplicationLanguage() {

        return Locale.getDefault().toString();
    }

    public static void displaySnackbar(View view, String message) {

        displaySnackbar(view, message, null, null);
    }

    public static void displaySnackbar(View view, String message, String actionText,
                                       View.OnClickListener onClickListener) {

        if (view == null)
            return;

        Context context = view.getContext();

        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);

        View viewSnackbar = snackbar.getView();

        TextView textSnackbar = (TextView) viewSnackbar.findViewById(android.support.design.R.id.snackbar_text);

        textSnackbar.setTextColor(ContextCompat.getColor(context, android.R.color.white));

        if (actionText != null && onClickListener != null) {

            snackbar.setActionTextColor(ContextCompat.getColor(context, R.color.colorAccent));

            snackbar.setAction(actionText, onClickListener);

            snackbar.setDuration(Snackbar.LENGTH_INDEFINITE);
        }

        snackbar.show();
    }

    public static String formatDate(long milis) {

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(milis);

        return DateFormat.getDateTimeInstance().format(cal.getTime());
    }

    public static String formatDateISO(long milis) {

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(milis);

        DateFormat df = new SimpleDateFormat(AppConstants.DATE_FORMAT);

        return df.format(cal.getTime());
    }


    public static String objectToJsonString(Object object) {

        String json;

        try {

            json = mapper.writeValueAsString(object);

        } catch (JsonProcessingException e) {
            json = "";
        }

        return json;

    }

}
