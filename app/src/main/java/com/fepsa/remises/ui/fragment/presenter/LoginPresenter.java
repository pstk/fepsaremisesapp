package com.fepsa.remises.ui.fragment.presenter;

import com.fepsa.remises.R;
import com.fepsa.remises.business.data.backend.util.ServerNotAvailableException;
import com.fepsa.remises.ui.application.App;
import com.fepsa.remises.ui.fragment.view.LoginView;

import rx.Subscriber;

/**
 * Created by tksko on 4/12/2017.
 */

public class LoginPresenter extends BasePresenter<LoginView> {

    public LoginPresenter() {

    }

    public void loginChofer(String nombre, String clave, String dni) {

        App.businessManager.loginChofer(nombre, clave, dni).subscribe(new Subscriber<Boolean>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

                if (e instanceof ServerNotAvailableException) {
                    view.onError(App.applicationContext.getString(R.string.msg_error_login));
                } else {
                    view.onError(e.getMessage());
                }

            }

            @Override
            public void onNext(Boolean success) {

                view.onSuccessLogin();

            }

        });

    }


}
