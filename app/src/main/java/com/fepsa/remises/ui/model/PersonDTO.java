package com.fepsa.remises.ui.model;

import android.support.annotation.NonNull;

import com.fepsa.remises.business.data.database.model.RealmPerson;

import java.io.Serializable;

/**
 * Created by nicolas.m on 11/6/2017.
 */

public class PersonDTO implements Serializable, Comparable<PersonDTO> {

    private long id;

    private String name;

    private String legajo;

    private boolean local;

    private int version;

    public PersonDTO(long id, String name) {
        this.id = id;
        this.name = name;
        this.local = true;
        this.legajo = String.valueOf(id);
        this.version = -1;
    }

    public PersonDTO(RealmPerson person) {
        this.id = person.getId();
        this.name = person.getName();
        this.legajo = person.getLegajo();
        this.local = person.isLocal();
        this.version = person.getVersion();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isLocal() {
        return local;
    }

    public void setLocal(boolean local) {
        this.local = local;
    }

    public String getLegajo() {
        return legajo;
    }

    public void setLegajo(String legajo) {
        this.legajo = legajo;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @Override
    public int compareTo(@NonNull PersonDTO o) {

        return this.getName().compareTo(o.getName());
    }
}
