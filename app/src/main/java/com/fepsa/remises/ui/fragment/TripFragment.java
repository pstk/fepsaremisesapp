package com.fepsa.remises.ui.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fepsa.remises.R;
import com.fepsa.remises.business.services.FusedLocationService;
import com.fepsa.remises.ui.activity.HomeActivity;
import com.fepsa.remises.ui.adapter.recyler_view_adapter.PassengersAdapter;
import com.fepsa.remises.ui.application.App;
import com.fepsa.remises.ui.fragment.presenter.TripFragmentPresenter;
import com.fepsa.remises.ui.fragment.view.TripFragmentView;
import com.fepsa.remises.ui.model.PersonDTO;
import com.fepsa.remises.ui.model.TripDTO;
import com.fepsa.remises.ui.util.BaseFunctions;
import com.fepsa.remises.ui.util.ColorUtils;
import com.fepsa.remises.ui.util.GpsUtils;
import com.fepsa.remises.ui.util.GraphicUtils;
import com.fepsa.remises.util.TripUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by nicolas.m on 10/31/2017.
 */

public class TripFragment extends BaseFragment<TripFragmentPresenter> implements PassengersAdapter.PassengerActionListener, TripFragmentView {

    @BindView(R.id.fragment_trip_button_end)
    AppCompatButton buttonEnd;

    @BindView(R.id.fragment_trip_button_cancel)
    AppCompatButton buttonCancel;

    @BindView(R.id.fragment_trip_recycler_view_passengers)
    RecyclerView passengersList;

    @BindView(R.id.fragment_trip_empty_view_passengers)
    View emptyView;

    private PassengersAdapter adapter;

    public TripFragment() {
        // Required empty public constructor
    }

    public static TripFragment newInstance() {

        TripFragment fragment = new TripFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        presenter = new TripFragmentPresenter();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_trip, container, false);

        bkUnbinder = ButterKnife.bind(this, view);

        updateButton();

        setupResults();

        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        presenter.setView(this, this);

    }

    private void updateButton() {

        buttonEnd.setEnabled(true);
        buttonCancel.setEnabled(true);

        int color = ContextCompat.getColor(App.applicationContext, R.color.red);

        int colorEnabled = color;

        int colorPressed = ColorUtils.darkenColor(color, 0.20d);

        int colorDisabled = ContextCompat.getColor(App.applicationContext, R.color.button_disabled);

        buttonEnd.setSupportBackgroundTintList(GraphicUtils.getColorStateList(
                colorEnabled,
                colorPressed,
                colorDisabled
        ));

        buttonCancel.setSupportBackgroundTintList(GraphicUtils.getColorStateList(
                colorEnabled,
                colorPressed,
                colorDisabled
        ));

    }

    private void setupResults() {

        adapter = new PassengersAdapter(true, this);

        passengersList.setAdapter(adapter);

        passengersList.setHasFixedSize(true);

        setEmptyViewVisibility();

    }

    @Override
    public void onResume() {

        super.onResume();

        getActivity().startService(new Intent(getActivity(), FusedLocationService.class));

        presenter.getPassengers();

    }

    private void setEmptyViewVisibility() {

        boolean visible = adapter.isEmpty();

        emptyView.setVisibility(visible ? View.VISIBLE : View.GONE);

        passengersList.setVisibility(visible ? View.GONE : View.VISIBLE);

    }

    @OnClick(R.id.fragment_trip_button_end)
    public void onButtonEndClick() {

        ((HomeActivity) getActivity()).navigateClsoeTrip();

    }

    @OnClick(R.id.fragment_trip_button_cancel)
    public void onButtonCancelClick() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.fragment_trip_confirm_title);

        builder.setMessage(getString(R.string.fragment_trip_confirm_cancel_trip_message));

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {

                presenter.cancelTrip();

                dialog.dismiss();

            }
        });

        builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();

        alert.show();

    }

    @OnClick(R.id.fragment_trip_button_passengers)
    public void onButtonPassengersClick() {

        ((HomeActivity) getActivity()).navigatePassengers();

    }

    @Override
    public void onPassengerAction(Object passenger) {

        PersonDTO person = (PersonDTO) passenger;

        if (GpsUtils.isGpsON(getActivity())) {
            presenter.removePassenger(person.getLegajo(), getLastKnownLocation());
        } else {
            GpsUtils.showGPSAlert(getActivity());
        }

        setEmptyViewVisibility();

    }

    @Override
    public void onError(String message) {

        BaseFunctions.displaySnackbar(getView(), message);

    }

    @Override
    public void onSuccessGetTrip(TripDTO trip) {

    }

    @Override
    public void onSuccessCreateTrip(TripDTO trip) {

    }

    @Override
    public void onSuccessCloseTrip(TripDTO trip) {

    }

    @Override
    public void onSucessActionPassenger(Object person) {

    }

    @Override
    public void onErrorActionPassenger(Object person) {

    }

    @Override
    public void onSuccessGetPassengers(List passengers) {

        adapter.updateList(passengers);

        setEmptyViewVisibility();

    }

    @Override
    public void onSuccessRemoveTrip() {

        getActivity().stopService(new Intent(getActivity(), FusedLocationService.class));

        TripUtils.getInstance().clearActiveTrip();

        ((HomeActivity) getActivity()).navigateHome();

    }

    @Override
    public void onSucessGetPersons(List persons) {

    }

}
