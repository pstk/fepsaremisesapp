package com.fepsa.remises.ui.fragment.presenter;

/**
 * Created by tksko on 02/07/2017.
 */

public interface Presenter {

    void onResume();

    void onPause();

    void onDestroy();

}