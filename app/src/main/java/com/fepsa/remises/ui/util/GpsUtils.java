package com.fepsa.remises.ui.util;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;

import com.fepsa.remises.ui.application.App;

/**
 * Created by tksko on 14/3/2018.
 */
public class GpsUtils {

    public static boolean isGpsON() {

        return isGpsON(App.applicationContext);
    }

    public static boolean isGpsON(Context context) {

        try {

            int locationCode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            return locationCode != 0;

        } catch (Settings.SettingNotFoundException e) {

            return true;

        }

    }

    public static void showGPSAlert(final Context context) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle("GPS");
        alertDialog.setMessage("El GPS se encuentra deshabilitado. Debe habilitarlo para poder obtener la posición correctamente.");
        alertDialog.setPositiveButton("Configuración", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                context.startActivity(intent);
                dialog.cancel();
            }
        });
        alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();

    }

}
