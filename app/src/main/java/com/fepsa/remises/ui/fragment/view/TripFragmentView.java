package com.fepsa.remises.ui.fragment.view;

import com.fepsa.remises.ui.model.TripDTO;

import java.util.List;

/**
 * Created by tksko on 02/07/2017.
 */

public interface TripFragmentView extends BaseView {

    void onSuccessGetTrip(TripDTO trip);

    void onSuccessCreateTrip(TripDTO trip);

    void onSuccessCloseTrip(TripDTO trip);

    void onSucessActionPassenger(Object person);

    void onErrorActionPassenger(Object person);

    void onSuccessGetPassengers(List passengers);

    void onSuccessRemoveTrip();

    void onSucessGetPersons(List persons);

}