package com.fepsa.remises.ui.fragment.presenter;

import android.content.Context;
import android.location.Location;

import com.crashlytics.android.Crashlytics;
import com.fepsa.remises.business.data.database.model.RealmLocation;
import com.fepsa.remises.business.data.database.model.RealmPassenger;
import com.fepsa.remises.business.data.database.model.RealmPerson;
import com.fepsa.remises.business.data.database.model.RealmTrip;
import com.fepsa.remises.data_synchronization.service.OnDemanUploadTripService;
import com.fepsa.remises.ui.application.App;
import com.fepsa.remises.ui.fragment.view.TripFragmentView;
import com.fepsa.remises.ui.model.PersonDTO;
import com.fepsa.remises.ui.model.TripDTO;
import com.fepsa.remises.util.ParametrosUtils;
import com.fepsa.remises.util.TripUtils;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;

/**
 * Created by nicolas.m on 11/3/2017.
 */

public class TripFragmentPresenter extends BasePresenter<TripFragmentView> {

    private long tripId;

    public TripFragmentPresenter() {
        this.tripId = TripUtils.getInstance().getActiveTripId();
    }

    public TripFragmentPresenter(long tripId) {
        this.tripId = tripId;
    }

    public void newTrip(long startDate, Location location) {

        RealmTrip trip = new RealmTrip();
        trip.setStartDate(startDate);

        RealmLocation theLocation = new RealmLocation(location, startDate, ParametrosUtils.getInstance().getVersion(), "Comienzo");

        addSubscription(App.businessManager.getDatabaseManager().saveTrip(trip, theLocation).subscribe(new Subscriber<RealmTrip>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        view.onError(e.getMessage());

                        Crashlytics.logException(e);

                    }

                    @Override
                    public void onNext(RealmTrip trip) {

                        view.onSuccessCreateTrip(new TripDTO(trip));

                    }

                })
        );

    }

    public void closeTrip(long nroViaje, int minutes, double peajes, double ticket, Location location) {

        RealmTrip trip = new RealmTrip();

        // Trip ID is the startDate
        trip.setStartDate(tripId);

        trip.setNroViaje(nroViaje);
        trip.setMinutes(minutes);
        trip.setPeajes(peajes);
        trip.setTicket(ticket);

        RealmLocation theLocation = new RealmLocation(location, tripId, ParametrosUtils.getInstance().getVersion(), "Fin");

        // Close trip
        trip.setEndDate(System.currentTimeMillis());

        addSubscription(App.businessManager.getDatabaseManager().saveTrip(trip, theLocation).subscribe(new Subscriber<RealmTrip>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        view.onError(e.getMessage());
                        Crashlytics.logException(e);
                    }

                    @Override
                    public void onNext(RealmTrip trip) {

                        view.onSuccessCloseTrip(new TripDTO(trip));
                    }

                })
        );

    }

    public void getPassengers() {

        addSubscription(App.businessManager.getDatabaseManager().getPassengers(tripId).subscribe(new Subscriber<List<RealmPassenger>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        view.onError(e.getMessage());
                        Crashlytics.logException(e);
                    }

                    @Override
                    public void onNext(List<RealmPassenger> realmPassengers) {

                        List<PersonDTO> people = new ArrayList<>();

                        for (RealmPassenger passenger : realmPassengers) {
                            people.add(new PersonDTO(passenger.getPerson()));
                        }

                        view.onSuccessGetPassengers(people);

                    }

                })
        );

    }

    public void addPassenger(PersonDTO person, Location location) {

        final RealmPassenger passenger = new RealmPassenger();

        passenger.setPerson(new RealmPerson(person));

        passenger.setPosSube(new RealmLocation(location, tripId, ParametrosUtils.getInstance().getVersion(), "SubeP"));

        addSubscription(App.businessManager.getDatabaseManager().addPassenger(tripId, passenger).subscribe(new Subscriber<RealmPassenger>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        view.onError(e.getMessage());

                        view.onErrorActionPassenger(passenger);

                    }

                    @Override
                    public void onNext(RealmPassenger passenger) {

                        view.onSucessActionPassenger(passenger);
                    }

                })
        );

    }

    public void removePassenger(String personLegajo, Location location) {

        RealmLocation bajaLocation = new RealmLocation(location, tripId, ParametrosUtils.getInstance().getVersion(), "BajaP");

        addSubscription(App.businessManager.getDatabaseManager().removepassenger(tripId, personLegajo, bajaLocation).subscribe(new Subscriber<RealmPassenger>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        view.onError(e.getMessage());
                        Crashlytics.logException(e);
                    }

                    @Override
                    public void onNext(RealmPassenger passenger) {

                        view.onSucessActionPassenger(passenger);
                    }

                })
        );

    }

    public void cancelTrip() {

        addSubscription(App.businessManager.getDatabaseManager().removeTrip(tripId).subscribe(new Subscriber<Boolean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        view.onError(e.getMessage());

                        Crashlytics.logException(e);
                    }

                    @Override
                    public void onNext(Boolean success) {

                        view.onSuccessRemoveTrip();

                    }

                })
        );
    }


    public void searchPersons(String query, boolean forceBackend) {

        addSubscription(App.businessManager.searchPersons(query, forceBackend).subscribe(new Subscriber<List<PersonDTO>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        Crashlytics.logException(e);

                    }

                    @Override
                    public void onNext(List<PersonDTO> personDTOs) {

                        view.onSucessGetPersons(personDTOs);

                    }

                })
        );

    }

    public void uploadTrips(Context context) {

        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(context));

        if (!OnDemanUploadTripService.isRunning()) {

            dispatcher.mustSchedule(dispatcher.newJobBuilder()
                    .setService(OnDemanUploadTripService.class) // the JobService that will be called
                    .setTag(OnDemanUploadTripService.class.getSimpleName())        // uniquely identifies the job
                    .build());

        }

    }

}
