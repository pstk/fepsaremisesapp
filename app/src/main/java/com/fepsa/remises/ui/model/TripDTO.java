package com.fepsa.remises.ui.model;

import com.fepsa.remises.business.data.database.model.RealmTrip;

import java.io.Serializable;

/**
 * Created by nicolas.m on 11/3/2017.
 */

public class TripDTO implements Serializable {

    private Long startDate;

    private Long endDate;

    private int minutes;

    private double peajes;

    private long nroViaje;

    private double ticket;

    public TripDTO() {

        this.startDate = null;
        this.endDate = null;
        this.minutes = 0;
        this.peajes = 0;
        this.nroViaje = 0;
        this.ticket = 0;
    }

    public TripDTO(RealmTrip trip) {

        this.startDate = trip.getStartDate();
        this.endDate = trip.getEndDate();
        this.minutes = trip.getMinutes();
        this.peajes = trip.getPeajes();
        this.nroViaje = trip.getNroViaje();
        this.ticket = trip.getTicket();
    }


    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    public Long getId() {
        return startDate;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public double getPeajes() {
        return peajes;
    }

    public void setPeajes(double peajes) {
        this.peajes = peajes;
    }

    public long getNroViaje() {
        return nroViaje;
    }

    public void setNroViaje(long nroViaje) {
        this.nroViaje = nroViaje;
    }

    public double getTicket() {
        return ticket;
    }

    public void setTicket(double ticket) {
        this.ticket = ticket;
    }
}
