package com.fepsa.remises.ui.adapter.recyler_view_adapter;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fepsa.remises.R;
import com.fepsa.remises.business.data.database.model.RealmLocation;
import com.fepsa.remises.ui.adapter.holder.GpsDataViewHolder;
import com.fepsa.remises.ui.util.BaseFunctions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tksko on 02/07/2017.
 */
public class GpsDataAdapter extends RecyclerView.Adapter<GpsDataViewHolder> {

    private List<RealmLocation> list;

    public GpsDataAdapter(List<RealmLocation> locations) {

        this.list = locations;

    }

    @Override
    public GpsDataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_gps_data, parent, false);

        return new GpsDataViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final GpsDataViewHolder holder, final int position) {

        final RealmLocation item = list.get(position);

        if (item != null) {

            holder.date.setText(BaseFunctions.formatDate(item.getTime()));

            holder.speed.setText(String.valueOf(item.getSpeed()) + "m/s");

            holder.bearing.setText(String.valueOf(item.getBearing()) + "°");

            holder.accuracy.setText(String.valueOf(item.getAccuracy()));

            if (!TextUtils.isEmpty(item.getInfo())) {
                holder.extraInfo.setVisibility(View.VISIBLE);
                holder.extraInfo.setText(item.getInfo());
            } else {
                holder.extraInfo.setVisibility(View.GONE);
            }

        }

    }

    @Override
    public int getItemCount() {

        return list == null ? 0 : list.size();
    }

}