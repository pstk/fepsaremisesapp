package com.fepsa.remises.config;

/**
 * Created by tksko on 02/07/2017.
 */

public class AppConstants {

    public static final String APP_TAG = "TestGPS";

    public static final String GOOGLE_SNAP_TO_ROAD_KEY = "AIzaSyCpig99Hhi4XN6BriDFgIgzyvcvnpWqxjE";

    public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    public static class Endpoint {

        public static final String EMPTY = "";

        public static final String GOOGLE_SNAP_TO_ROAD = "https://roads.googleapis.com/v1/snapToRoads";

        public static final String UPDATE_PERSONAS = "/posibles-pasajeros";

        public static final String UPDATE_PARAMETERS = "/app-info";

        public static final String LOGIN_CHOFER = "/activar-chofer";

        public static final String UPLOAD_TRIP = "/viajes";

    }

    public static class BroadcastActions {

        public static final String NO_CONNECTION = "NO_CONNECTION";

        public static final String NEW_LOCATION = "FEPSA_NEW_LOCATION";

        public static final String PENDING_TRIPS = "FEPSA_PENDING_TRIPS";
    }

}
